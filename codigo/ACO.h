#ifndef ACO_H
#define ACO_H

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cmath>
#include <random>
//#include <cstdlib>

using namespace std;

    #define MASK 2147483647
    #define PRIME 65539
    #define SCALE 0.4656612875e-9

    void Set_random (unsigned long x);
    float Rand(void);
    int Randint(int low, int high);
    float Randfloat(float low, float high);


struct Elem
{
    char a;
    int i;
    int j;
};

class Ant {
    public:
        
        vector<string> input;
        vector<int> stateVector;
        string solution;
        multimap<char,float> candidates;
        int id;
        int rank;
        float totalPheromone;
        float q0;
        float alpha;
        
        vector<vector<Elem>> M;
        vector<vector<float>> updatePheromones;

    
    

        Ant(vector<string> input, int id);
        void select_candidates(vector<vector<float>> pheromones, vector<Elem>& M_aux);
        void select_candidatesLMM(vector<vector<float>> pheromones, vector<Elem>& M_aux);
        char select_max_pheromone_char(vector<Elem> M_aux, vector<Elem>& M_select);
        void update_state_vector(char a);
};


class ACO {
    private:

        vector<string> input;
        vector<vector<float>> pheromones;
        vector<vector<float>> updatePheromones;
        int m;
        vector<int> finishState;
        vector<Ant> ants;
        float p;
        float y;

    public:

    ACO(vector<string> input);
    void calcule_rank();
    int function_rank(int rank);
    string run();
};




#endif