#ifndef POEMS_H
#define POEMS_H

#include<iostream>
#include<vector>
#include<map>
#include<set>
#include <algorithm>
#include <functional>
#include <numeric>
#include "MM.h"
using namespace std;

//void Set_random (unsigned long x);
//float Rand(void);
//int Randint(int low, int high);

//////////////////////////////////////////////////////////////////////////////////////////

struct action {
    char typeAction;
    int id1;
    int id2;
    char letter;
};

//////////////////////////////////////////////////////////////////////////////////////////

class GA {
  private:
    int maxGen;
    int N;
    string prototype;
    vector<vector<action>> pop;
    vector<vector<vector<action>>> niches;
    vector<multimap<float, vector<action>>> orderedNiches;
    vector<float> eval;
    vector<vector<float>> nichingEval;
    string alphabet;
    vector<string> input;
    int sumImput;
    float Pcross;
    float Pmut;

  public:
    GA(int maxGen, int N, string prototype, string alphabet, vector<string> input);
    action random_action(int limit);
    vector<action> random_chromosome();
    void initialize_pop();
    void visualize_pop();
    int alignMod(string cad);
    int alignMod(string cad, string prot);
    float fitness();
    float fitness(string prot);
    string apply_action(action a);
    string apply_action(action a, string prot);
    string apply_seq_actions(vector<action> seq, string prot);
    float evaluate_chromosome(vector<action> seq);
    void evaluate_pop();
    vector<action> best_sequence();
    vector<action> select();
    vector<action> cross_over(vector<action> p1, vector<action> p2);
    void mutate(vector<action>& child);
    string run_genericGA();
    
    action random_active_action(int limit);
    vector<action> random_chromosome_niche(int i);
    void initialize_niches();
    void evaluate_niches();
    vector<action> niching_tournament_selection();
    void cross_over(vector<action> p1, vector<action> p2, vector<action>& child1, vector<action>& child2);
    void niching_replacement(vector<action> child);
    vector<action> best_sequence_niching();
    string run_iEA();
};


class POEMS {
  private:    
    string prototype;
    vector<string> input;
    string alphabet;
    int sumImput;


  public:
    POEMS(vector<string> input, string alphabet);
    float fitness(string prot);
    int alignMod(string cad, string prot);
    string apply_action(action a, string prot);
    string run();
};

#endif