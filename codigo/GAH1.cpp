#include "GAH1.h"

//////////////////////////////////////////////////////////////////////////////////////////
#include<ctime>
//Metodos random:

#define MASK 2147483647
#define PRIME 65539
#define SCALE 0.4656612875e-9

unsigned long Seed = 0L;
void Set_random (unsigned long x){
    Seed = (unsigned long) x;
}
float Rand(void){
    return (( Seed = ( (Seed * PRIME) & MASK) ) * SCALE );
}
int Randint(int low, int high){
    return (int) (low + (high-(low)+1) * Rand());
}
float Randfloat(float low, float high){
    return (low + (high-(low))*Rand());
}

//////////////////////////////////////////////////////////////////////////////////////////

GAH1::GAH1(vector<string> input, string alphabet){
    this->input = input;
    this->sumInput = 0;
    for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++)
        this->sumInput += it->size();
    this->alphabet = alphabet;
    this->Pcross = 1;
    this->Pmut = 0.02;
}

//////////////////////////////////////////////////////////////////////////////////////////

void GAH1::normalize_sum_1(vector<float>& chr){
    //Calculamos la suma de todos los valores del cromosoma:
    float sum = 0;
    for(auto it = chr.begin(); it != chr.end(); it++)
        sum += *it;
    //Dividimos cada gen por este valor:
    for(int i=0; i<chr.size(); i++)
        chr[i] = chr[i]/sum;
}

//////////////////////////////////////////////////////////////////////////////////////////

void GAH1::initialize_pop(){
    vector<float> chromosome;
    vector<vector<float>> subpop;
    //multimap<int, vector<float> , greater<int>> subpop;

    int nChr = 150;
    int nSubpop = 10; 

    for(int i=0; i<nSubpop; i++){
        for(int j=0; j<nChr; j++){            
            // Cromosoma random:
            for(int k=0; k<this->sumInput; k++){
                chromosome.push_back(Rand());
            }            
            // Normalizamos cromosoma sum 1:
            this->normalize_sum_1(chromosome);
            // Añadimos cromosoma a la subpop, con eval=0 provisional, hasta que en la siguiente fase se calculen las eval:
            //pair<int , vector<float>> aux (0,chromosome);
            //subpop.insert(aux); 
            subpop.push_back(chromosome);           
            chromosome.clear();
        }        
        // Añadimos subpop a la pop
        this->pop_aux.push_back(subpop);
        subpop.clear();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

int GAH1::evaluate_chr(vector<float> chr){

    // SE EJECUTARA LA HEURISTICA H1 MODIFICADA:
    // Representación de los parametros en forma de matriz:
    vector<vector<float>> params;
    vector<string> in = this->input;
    int pos_chr = 0;
    for(int i=0; i<in.size(); i++){
        vector<float> aux;
        for(int j=0; j<in[i].size(); j++){
            aux.push_back(chr[pos_chr]);
            pos_chr++;
        }
        params.push_back(aux);
    }


    string solucion;

    map<char,float> pesos;

    for(auto it = this->alphabet.begin(); it != this->alphabet.end(); it++){
        pesos[*it] = 0;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    cout << "==========> PESOS: " << endl;
        for(auto it = pesos.begin(); it != pesos.end(); it++)
            cout << it->first << ": " << it->second << endl;
    */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    typedef function<bool(pair<char, float>, pair<char, float>)> Comparator;
 
	Comparator compFunctor = [](pair<char, float> elem1 ,pair<char, float> elem2){
		return elem1.second > elem2.second;
	};
    

    bool isEmpty = false;

    //H1:
    while(!isEmpty){
        
        //Contamos los pesos:
        for(int i=0; i<in.size(); i++){
            if(!(in[i].empty()))
                pesos[in[i][0]] = pesos[in[i][0]] + (in[i].size() * params[i][0]);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*
        cout << "==========> PESOS: " << endl;
        for(auto it = pesos.begin(); it != pesos.end(); it++)
            cout << it->first << ": " << it->second << endl;
        */
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     

        //Creamos un multiset ordenado con los valores del map frecuencias, de esta forma el primer elemento del multiset
        // (*.begin()) será el caracter con mayor frecuencia:
        multiset<pair<char, float>, Comparator> pesosOrdenados(pesos.begin(), pesos.end(), compFunctor);
        
        //Seleccionamos el caracter con mayor frecuencia:
        char masPeso = pesosOrdenados.begin()->first;

        //Lo añadimos al superstring:
        solucion.push_back(masPeso);
        //cout << "------->"<< masPeso << endl;
        //Lo eliminamos del front de las cadenas de entrada:
        for(int i=0; i<in.size(); i++){
            
            if(!in[i].empty()){ 
                if(in[i][0] == masPeso){
                    /*
                    cout << "CAracter del input: " << in[i][0] << endl;
                    cout << "Mas peso: " << masPeso << endl;
                    cout << "Parametro: " << params[i][0] << endl; 
                    */
                    in[i].erase(in[i].begin());
                    params[i].erase(params[i].begin());                
                }
            }
        }
                      

        //Comprobamos que todavía quedan carecteres en alguna cadena:    
        isEmpty = true;
        for(vector<string>::iterator it = in.begin(); it != in.end(); it++)
            if(!(it->empty()))
                isEmpty = false;

        //Limpiamos las estructuras de datos necesarias:

        for(string::iterator it = this->alphabet.begin(); it != this->alphabet.end(); it++){
            pesos[*it] = 0;
        }
    }
    return solucion.size();
}

//////////////////////////////////////////////////////////////////////////////////////////

void GAH1::evaluate_pop(){
    for(int i=0; i<pop_aux.size(); i++){
        multimap<int, vector<float>> subpop;
        for(int j=0; j<pop_aux[i].size(); j++){
            pair<int , vector<float>> aux (this->evaluate_chr(pop_aux[i][j]),pop_aux[i][j]);
            subpop.insert(aux);
            //aux.push_back(this->evaluate_chr(pop[i][j]));
        }
        this->pop.push_back(subpop);
        //this->eval.push_back(aux);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

void GAH1::select(vector<float>& p, int subpopIndex){
    
    int pos1 = Randint(0, this->pop[subpopIndex].size()-1);
    int pos2 = Randint(0, this->pop[subpopIndex].size()-1);
    auto it1 = this->pop[subpopIndex].begin();
    auto it2 = this->pop[subpopIndex].begin(); 
    for(int i=0; i<pos1; i++){
        it1++;
    }
    for(int i=0; i<pos2; i++){
        it2++;
    }
    if(it1->first <= it2->first)
        p = it1->second;
    else
        p = it2->second;
}

//////////////////////////////////////////////////////////////////////////////////////////

vector<float> GAH1::cross_over(vector<float> p1, vector<float> p2){
    int n1 = Randint(0,this->sumInput-1);
    int n2 = Randint(0,this->sumInput-1);
    if(n2 < n1){
        int aux = n1;
        n1 = n2;
        n2 = aux;
    }

    vector<float> child;
    for(int i=0; i<n1; i++)
        child.push_back(p1[i]);
    for(int i=n1; i<n2; i++)
        child.push_back(p2[i]);
    for(int i=n2; i<p1.size(); i++)
        child.push_back(p1[i]);

    this->normalize_sum_1(child);
    return child;
}

//////////////////////////////////////////////////////////////////////////////////////////

void GAH1::mutate(vector<float>& child){

    int n = rint(this->Pmut * child.size());
    set<int> duplicate;
    for(int i=0; i<n; i++){
        //float aux = distribucionNormal();
        float aux = Randfloat(0,1);
        int pos = Randint(0,child.size()-1);
        // Mientras pos este en el set(ya ha sido elegida):
        while(duplicate.find(pos) != duplicate.end()){
            pos = Randint(0,child.size()-1);
        }
        duplicate.insert(pos);

        child[pos] = child[pos] + aux; 
    }
    this->normalize_sum_1(child);
}

//////////////////////////////////////////////////////////////////////////////////////////

void GAH1::replacement(vector<float> child, int subpopIndex){

    //Eliminamos el ultimo individio de la pop que sera el peor valorado(la seq mas larga):
    this->pop[subpopIndex].erase(--this->pop[subpopIndex].end());
    
    //Añadimos el hijo:
    int ev = this->evaluate_chr(child);
    pair<int,vector<float>> aux (ev , child);
    this->pop[subpopIndex].insert(aux);
}

//////////////////////////////////////////////////////////////////////////////////////////

string GAH1::get_sequence(vector<float> chr){
    // SE EJECUTARA LA HEURISTICA H1 MODIFICADA:
    // Representación de los parametros en forma de matriz:
    vector<vector<float>> params;
    vector<string> in = this->input;
    int pos_chr = 0;
    for(int i=0; i<in.size(); i++){
        vector<float> aux;
        for(int j=0; j<in[i].size(); j++){
            aux.push_back(chr[pos_chr]);
            pos_chr++;
        }
        params.push_back(aux);
    }


    string solucion;

    map<char,float> pesos;

    for(auto it = this->alphabet.begin(); it != this->alphabet.end(); it++){
        pesos[*it] = 0;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    cout << "==========> PESOS: " << endl;
        for(auto it = pesos.begin(); it != pesos.end(); it++)
            cout << it->first << ": " << it->second << endl;
    */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    typedef function<bool(pair<char, float>, pair<char, float>)> Comparator;
 
	Comparator compFunctor = [](pair<char, float> elem1 ,pair<char, float> elem2){
		return elem1.second > elem2.second;
	};
    

    bool isEmpty = false;

    //H1:
    while(!isEmpty){
        
        //Contamos los pesos:
        for(int i=0; i<in.size(); i++){
            if(!(in[i].empty()))
                pesos[in[i][0]] = pesos[in[i][0]] + (in[i].size() * params[i][0]);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*
        cout << "==========> PESOS: " << endl;
        for(auto it = pesos.begin(); it != pesos.end(); it++)
            cout << it->first << ": " << it->second << endl;
        */
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     

        //Creamos un multiset ordenado con los valores del map frecuencias, de esta forma el primer elemento del multiset
        // (*.begin()) será el caracter con mayor frecuencia:
        multiset<pair<char, float>, Comparator> pesosOrdenados(pesos.begin(), pesos.end(), compFunctor);
        
        //Seleccionamos el caracter con mayor frecuencia:
        char masPeso = pesosOrdenados.begin()->first;

        //Lo añadimos al superstring:
        solucion.push_back(masPeso);
        //cout << "------->"<< masPeso << endl;
        //Lo eliminamos del front de las cadenas de entrada:
        for(int i=0; i<in.size(); i++){
            
            if(!in[i].empty()){ 
                if(in[i][0] == masPeso){
                    /*
                    cout << "CAracter del input: " << in[i][0] << endl;
                    cout << "Mas peso: " << masPeso << endl;
                    cout << "Parametro: " << params[i][0] << endl; 
                    */
                    in[i].erase(in[i].begin());
                    params[i].erase(params[i].begin());                
                }
            }
        }
                      

        //Comprobamos que todavía quedan carecteres en alguna cadena:    
        isEmpty = true;
        for(vector<string>::iterator it = in.begin(); it != in.end(); it++)
            if(!(it->empty()))
                isEmpty = false;

        //Limpiamos las estructuras de datos necesarias:

        for(string::iterator it = this->alphabet.begin(); it != this->alphabet.end(); it++){
            pesos[*it] = 0;
        }
    }
    return solucion;
}

//////////////////////////////////////////////////////////////////////////////////////////

string GAH1::best_sequence(){
    vector<float> best= this->pop.begin()->begin()->second;
    int min = 99999;
    for(auto subpop = this->pop.begin(); subpop != this->pop.end(); subpop++){
        if(subpop->begin()->first < min){
            best = subpop->begin()->second;
            min = subpop->begin()->first;
        }

    }
  
    return(this->get_sequence(best));
}

//////////////////////////////////////////////////////////////////////////////////////////

string GAH1::run(){
    int n_eval = 0;
    int migration = 0;
    int control =0;
    vector<float> best;
     unsigned t0,t1;
    double time;
    t0 = clock();
    this->initialize_pop();
    this->evaluate_pop();
    while(n_eval < 150000){
        //for(auto subpop = this->pop.begin(); subpop != this->pop.end(); subpop++){
        for(int i=0; i<this->pop.size(); i++){    
            vector<float> p1;
            vector<float> p2;
            this->select(p1,i);
            this->select(p2,i);
            vector<float> child = this->cross_over(p1,p2);
            this->mutate(child);
            this->replacement(child,i);
            
        }
        n_eval+=this->pop.size();
        migration++;
        if(migration == 10){
            int migrationIndex = Randint(0,this->pop.size()-1);
            vector<float> chReplace = this->pop[migrationIndex].begin()->second;
            
            for(int i=0; i<this->pop.size(); i++){
                if(i != migrationIndex){
                   this->replacement(chReplace, migrationIndex);
                }
            }
            migration=0;
        }

         if(control%100 == 0){
             //cout << "a";
             string x = this->best_sequence();
             //cout << x << endl;
             t1 = clock();
             time = (double(t1-t0)/CLOCKS_PER_SEC);
             if(time > 200) {
                 return (this->best_sequence());

             }
             cout << time << " " << x.size() << endl;
         }
         control++;

    }
    return (this->best_sequence());
}

//////////////////////////////////////////////////////////////////////////////////////////