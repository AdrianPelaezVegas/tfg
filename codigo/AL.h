#ifndef AL_H
#define AL_H

#include<iostream>
#include<vector>
#include<map>
#include<set>
#include <algorithm>
#include <functional>
#include <numeric>

using namespace std;

int myrandom (int i);

class AL {
    private:

        string solution;
        string alphabet;
        vector<string> input;
        vector<vector<int>> LEMatrix;
        string permutation;
        int maxInputSize;
        vector<int> productivity;

    public:

        AL(vector<string> input, string alphabet);
        void leftmost_embbeding();
        void calculate_productivity();
        void delete_unproductive_steps();
        string run();
};


#endif