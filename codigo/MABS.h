#ifndef MA_H
#define MA_H

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cmath>
#include <random>
//#include <cstdlib>
#include <ctime> 


//#include "MM.h"
#include "LS.h"

using namespace std;

class MA {
    private:
    
    vector<string> input;
    string alphabet;
    vector<string> pop;
    vector<int> eval;
    int LowerBound;
    int UpperBound;

    public:
    
    MA(vector<string> input, string alphabet);
    void initialize_random_pop();
    void initialize_BnB_pop();
    void repair_sol(string& solution);
    void repair_pop();
    int evaluate_sol(string sol);
    void evaluate_pop();
    string binary_tournament_selection();
    void uniform_cross_over(string p1, string p2, string& c1, string& c2);
    void mutate(string& c);
    vector<string> select_2_best(string p1, string p2, string c1, string c2);
    string best_of_pop();
    string run();
};







#endif