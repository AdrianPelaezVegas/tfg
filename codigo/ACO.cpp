#include "ACO.h"

//Metodos random:

      unsigned long Seed = 0L;

    void Set_random (unsigned long x){
        Seed = (unsigned long) x;
    }
    float Rand(void){
        return (( Seed = ( (Seed * PRIME) & MASK) ) * SCALE );
    }
    int Randint(int low, int high){
        return (int) (low + (high-(low)+1) * Rand());
    }
    float Randfloat(float low, float high){
        return (low + (high-(low))*Rand());
    }


Ant::Ant(vector<string> input, int id){
    this->input = input;
    vector<int> aux (this->input.size(), 0);
    this->stateVector = aux;
    this->id = id;
    for(auto it = this->input.begin(); it != this->input.end(); it++){
        vector<float> aux (it->size(), 0);
        this->updatePheromones.push_back(aux);
    }
    this->q0 = 0.5;
    this->alpha = 9;
}

void Ant::select_candidates(vector<vector<float>> pheromones, vector<Elem>& M_aux){
    this->candidates.clear();
    for(int i = 0; i < this->stateVector.size(); i++){
        if(this->stateVector[i] < this->input[i].size()){
            char a = this->input[i][this->stateVector[i]];

            if(this->candidates.count(a) == 0){
                pair<char,float> aux(a, pheromones[i][this->stateVector[i]]);
                this->candidates.insert(aux);            
            }
            else{
                auto it = this->candidates.find(a);
                it->second = it->second + pheromones[i][this->stateVector[i]];
            }
            Elem elem;
            elem.a = a;
            elem.i = i;
            elem.j = this->stateVector[i]; 
            M_aux.push_back(elem);
        }
    }
}

void Ant::select_candidatesLMM(vector<vector<float>> pheromones, vector<Elem>& M_aux){
    this->candidates.clear();
    for(int i = 0; i < this->stateVector.size(); i++){
        if(this->stateVector[i] < this->input[i].size()){
            char a = this->input[i][this->stateVector[i]];

            if(this->candidates.count(a) == 0){
                float value = pheromones[i][this->stateVector[i]] * (this->input[i].size() - this->stateVector[i] +1 );
                pair<char,float> aux(a, value);
                this->candidates.insert(aux);            
            }
            else{
                auto it = this->candidates.find(a);
                it->second = it->second + pheromones[i][this->stateVector[i]];
            }
            Elem elem;
            elem.a = a;
            elem.i = i;
            elem.j = this->stateVector[i]; 
            M_aux.push_back(elem);
        }
    }
}

char Ant::select_max_pheromone_char(vector<Elem> M_aux, vector<Elem>& M_select){

    multimap<float,char> order1;
    multimap<float,char> order2;
    float q = Randfloat(0,1);
    char charSelect;
    if (q < this->q0){
        for(auto it = this->candidates.begin(); it != this->candidates.end(); it++){
            pair<float,char> aux( pow(it->second, this->alpha) , it->first);
            order1.insert(aux);
        }

        auto it = order1.end();
        it = --it;
        charSelect = it->second;
    }
    else{
        float sum = 0;
        for(auto it = this->candidates.begin(); it != this->candidates.end(); it++){
            sum += pow(it->second, this->alpha);
        }
        for(auto it = this->candidates.begin(); it != this->candidates.end(); it++){
            float prob = pow(it->second, this->alpha) / sum;
            pair<float,char> aux(prob, it->first);
            order2.insert(aux);
        }

        auto it = order2.end();
        it = --it;
        charSelect = it->second;
    }
    for(auto elem = M_aux.begin(); elem != M_aux.end(); elem++){
        if(elem->a == charSelect){
            M_select.push_back(*elem);
        }
    }

    return charSelect;
}

void Ant::update_state_vector(char a){
    for(int i = 0; i < this->input.size(); i++){
        if(this->input[i][this->stateVector[i]] == a){
            this->stateVector[i]++;
        }
    }
}


ACO::ACO(vector<string> input){
    this->input = input;
    for(auto it = this->input.begin(); it != this->input.end(); it++){
        vector<float> aux (it->size(), 1);
        this->pheromones.push_back(aux);
    }
    this->m = 16;
    for(auto it = this->input.begin(); it != this->input.end(); it++){
        this->finishState.push_back(it->size());
    }
    
    for(int i = 0; i < this->m; i++){
        Ant ant (this->input, i);
        this->ants.push_back(ant);
    }

    for(auto it = this->input.begin(); it != this->input.end(); it++){
        vector<float> aux (it->size(), 0);
        this->updatePheromones.push_back(aux);
    }
    this->p = 0.5; 
    this->y = 1000;
    
}

void ACO::calcule_rank(){
    // multimap (size,id)
    multimap<int,int> order;
    for(auto ant = this->ants.begin(); ant != this->ants.end(); ant++){
        pair<int,int> aux(ant->solution.size(), ant->id);
        order.insert(aux);
    }
    int rank = 1;
    for(auto it = order.begin(); it != order.end(); it++){
        for(auto ant = this->ants.begin(); ant != this->ants.end(); ant++){
            if(ant->id == it->second){
                ant->rank = rank;
            }
        }
        rank++;
    }
}

int ACO::function_rank(int rank){
    if(rank < 9)
        return 1;
    else 
        return 0;
}

string ACO::run(){
    int stop = 0;
    string best;
    while(stop<100000){
        //Calcular solucion de cada hormiga

        // Hacer clear de las hormigas

        for(auto ant = this->ants.begin(); ant != this->ants.end(); ant++){
            while(ant->stateVector != this->finishState){
                //Calcular conjunto de candidatos -> caracteres en el front actual que fija ant->stateVector
                vector<Elem> M_aux;
                vector<Elem> M_select;
                ant->select_candidates(this->pheromones, M_aux);
                char a = ant->select_max_pheromone_char(M_aux,M_select);
                ant->M.push_back(M_select);
                ant->solution.push_back(a);
                ant->update_state_vector(a);
            }
            if( best.size() == 0 || best.size() > ant->solution.size() ){
                best = ant->solution;
            }
        }
        //Calcular update feromonas de cada hormiga
        
        //Calcular ranking
        this->calcule_rank();
        for(auto ant = this->ants.begin(); ant != this->ants.end(); ant++){
            //Calcular cantidad total de feromona a añadir por la hormiga:
            ant->totalPheromone = this->function_rank(ant->rank) * (1 / ant->solution.size());
            //Calcular cantidad de update de feromona de la hormiga que le corresponde a cada caracter
            int l = 1;
            for(auto it = ant->M.begin(); it != ant->M.end(); it++){
                for(auto it2 = it->begin(); it2 != it->end(); it2++){
                    ant->updatePheromones[it2->i][it2->j] = (ant->totalPheromone / it->size()) * 2 * (  (ant->solution.size() - l +1) / (ant->solution.size() * ant->solution.size() + ant->solution.size())  ); 
                }
                l++;
            }
        }

        //Calcular cantidad total de update para cada caracter del input:
        for(int i = 0; i < this->updatePheromones.size(); i++){
            for(int j = 0; j < this->updatePheromones[i].size(); j++){
                int sum = 0;
                for(auto ant = this->ants.begin(); ant != this->ants.end(); ant++){
                    sum += ant->updatePheromones[i][j];
                }
                this->updatePheromones[i][j] = sum;
            }
        }
        //Update el valor de feromona en cada caracter del input:
        for(int i = 0; i < this->pheromones.size(); i++){
            for(int j = 0; j < this->pheromones[i].size(); j++){
                this->pheromones[i][j] = (1 - this->p) * this->pheromones[i][j] + ( (this->y) * (this->updatePheromones[i][j]) );
            }
        }


    stop++;
    }

    return best;
}