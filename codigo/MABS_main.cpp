#include"MABS.h"


int main(){
    string solucion;

    string alphabet = "abcd";
    vector<string> input;

    unsigned t0, t1;
    double time;

vector<int> semillas;
    semillas.push_back(4);
    semillas.push_back(9834);
    semillas.push_back(3);
    semillas.push_back(155);
    semillas.push_back(846);
    semillas.push_back(980);
    semillas.push_back(34527);
    semillas.push_back(57);
    semillas.push_back(34);
    semillas.push_back(6345);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
for(int i = 0; i<10; i++){    

    input.clear();
    // => 158_10:
    input.push_back("acacbbadcbbdaadcbddaadaadccbbbdbddcddbddcbdbcbaaabadabbcbdcdaabddadbababbcdddbdabaccddabdaabcacddcbcbaacdaddaacdcacadccbadcdcdccbccbdbabdadadcda");
    input.push_back("bcacbbadcbbdaabadcbddacdaadcbbdbdddcddbddcbdbcbaaabadaabdcbdcdaabddadbababbcdddbdabacddacbdaabcadcdcbcbaacdaaacdcacadccdbadcdcdcbccdbabdadadcdda");
    input.push_back("bacacbbacbbdaabadcbddaccadadcbbdbdbdcddddcbdbcbaaabadaabaddcbdcdaabddadbababbcdddaaccddaaabcacdcdcbbaacdaddaadcacadcdaddcdccbccbdabddacdda");
    input.push_back("bacacbbadcbbaabadcbdaccaaadccbbdbdbddcddbddcbdbcbaabdababddcbdcdaabddadbababcdddbdabaccddacbdaabcacdcdcbcbaadaacdacadccdacdcdccbccdbbdadacdda");
    input.push_back("bacacbadcbbaadcbddaccadadccbbdbbddcddbddcbdbcbaaabadaababddcbddaabddadbaabbdddbdabaccddcbabcaccdcbcbacdaddacdcacadccdbadcdcdccbcbabdadddd");
    input.push_back("baccbbacbbdaabadcbddaccadaadccbbdbdbddcddbddcbdbcbaaabadaababddbcdabdddbababbcdddbdaaccddacbdaabcacdcdcbcaadaddaacdcacadccdbadcdcdccbccdbadadadcdd");
    input.push_back("baccbbadcbbdabacbddaccaadccbddbddcddbdcbdbcbaabadaababddcdcdaabddadbababbcdddbdbaccddacbdabacdcdcbcbacdaddaacdcacaccdbadcdcdccbccbbabdadadcdda");
    input.push_back("bacbbadbdaabadcbddaccdaadcbbdbdbdcddbddcdbcbaaaadababddcbdcdaabddadbbbbcdddbbaccddacbdaabcacdcdcbcbaacddacdcacadccdbdcdccbccdbabdaadcdda");
    input.push_back("bacacbbadbbdaabadcbddaccadaadccbbdbdbddcddbddcbdbcbaabaababddcbdcdaabddbbabbdddbdaacddacdaabcacdcdbcbaadaddaacdccadccdbaddcdccbccbdbabdaddcdda");
    input.push_back("acacbbadcbbdaabadcbdaccadadcbdbdbddcddbdbdbcbaaabdaababddcbdcdaabddadbababcddbdbaccddacbdabcacdcdcbcbaacdadacdcaadccdbadcdcdcbccbdbabdadadcdda");
        Set_random(semillas[i]);

    MA _MA1(input, alphabet);
    t0=clock();
    solucion = _MA1.run();
    t1=clock();
    time = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "158_10 => Fitness: " << solucion.size() << endl;
    
    cout << "Tiempo: " << time <<endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    input.clear();
    // => 158_15:
    input.push_back("acacbbadcbbdaadbddadaadccbbbdbddcddbdcbdbcbaaabadabbcbdcdaabddadbaabbcdddbdabaccddabdacacddcbcbaacdaddaacdcacaccbaddcdccbccbdbabdadadcda");
    input.push_back("bcacbbadbdaabdcbddadaacbdbdddcddbddcbdbcbaaabadaabdcbdcdaabddadbababbcdddbdabacddacbaabcadcdcbcaacdaaacdcacadccdbadcdccbccdbadadadcda");
    input.push_back("bacacbbacbdaabadcbddacadadcbdbdbcddddcbdbcbaaabdaaaddcbdcabddadbababbcdddaaccaaabcacdcdcbbaacdaddaadcacdcdaddcdccbcbdabddacda");
    input.push_back("bacacbbadbbaabadcbdccaaadccbbdbdbddcddbddcbdbcbaabdababddcbdcdaabddadbababcdddbdabaccddacbdabcacdccbcbaadaadacadccdacdcdccbccdbbdadacdda");
    input.push_back("bacacbadcbbaadcbddaccadadccbbdbbddcddddcbdbcbaaabadaababddcbddabddadbaabbdddbdabaccddcbabcaccdcbcbacdaddacdcacadccdbadcddccbcbabdaddd");
    input.push_back("baccbbacbdaabadcbdaccadaadccbbdbdbddcddbddcbdbcbaaabadaababddbcdabddbababbcdddbdaacddacbdaabcadcdbcaadadacdcacadccdbadcdcdccbcdbadadadcdd");
    input.push_back("baccbbadcbbdabacbddaccaadccbddbddcddbdbcbaabadaababddcdcdaabddadbababcdddbdbaccdacbdabacdcdcbcbacdaddaacdcacaccbadccdccbccbbbdadadcdda");
    input.push_back("acbbadbdaabadcbddaccdaadcbbdbdbdcddbddcdbcbaaaadababddcbdcdaabddadbbbbcddbbaccddacbdaabcacdcdcbcaacddcdcacadccdbdcdccbcdbabdadcdda");
    input.push_back("bacabbadbbdaabadcbddccadaadccbbdbdbddcddbddcbdbcbaabaababddcbdcdaabddbbabbdddbdaacddacdabcadcdbcbaadaddaacdccadccdbaddcdccbccbdbabdadcdda");
    input.push_back("acacbbadcbbdaabadbdaccadadcbdbdbddcddbdbdbcbaaabaababddcbdcdaabddadbababcddbdbccddabdabacdcdcbcbaacdadacdcaadccdbadcdcdcbccbdbabdadadcdda");
        Set_random(semillas[i]);

    MA _MA2(input, alphabet);
    solucion = _MA2.run();
    cout << "158_15 => Fitness: " << solucion.size() << endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    input.clear();
    // => 158_20:
    input.push_back("aacbbadcbdaadddadaadcbbbdbddcddbdcbdbcbaaabadabbcbdcdaabddadbaabbcddbdabaccdabdacacddccbadadaacdcacaccbaddcdccbccbdbabddadcda");
    input.push_back("bccbbdbdaabdcbddadaacbdbdddcddbddcdbcbabadaabdcbdcdaabddadbababcddddabacddacbaabcadcdcbaacaaacdcacaccdbadcdccbccdbadadadcda");
    input.push_back("bacacbbacbdaabadcbddcadadcbdbdbcdddcbdbcbaaabdaaaddcbdcabddadbababbcdddaaccaaabcacdcdcbbaacdaddaadcacdcdaddcdccbcbdabddacda");
    input.push_back("bacacbadbbabadcbdccaadcbbbdbddcdbddcbdbcbaadabddcbdcdabddabababcdddbdabaccddacbdabcacdccbcbaadaadacdccdacdcdccbccdbdadacdda");
    input.push_back("bacacbadcbbaadcbddaccadadccbbdbbddcddddcdbcbaabadababddbddabddabaabdddbdabaccddcabcaccdcbcbacdaddacdcacadccdbadcddcccbabdddd");
    input.push_back("baccbbacbdaabadcbdacadaadccbbdbdbddcddbddcbdbcbaaabadababddbcdabdbababbcddbdaacddacbdaabcadcdbcaaadacdcacadccdbdddcbdbadadadcdd");
    input.push_back("baccbbadcbdabacbddaccaadccddbddcddbdbcbaabadaababddcdabddadbababcddbdbaccdacbdabaccdcbcbacdaddaacdcacaccbadccdccbccbbbdadadcdda");
    input.push_back("acbbadbdaabadcbddaccdaadcbbdbdbdcddbddcdbcbaaaadababddcbdcdaabdadbbbbddbbaccddacbdaabcacdcdcbcaaddcdcacadccdbdcdccdbaddcdda");
    input.push_back("bacabadbbdaabadcbddcadaadccbbdbdbddcddddcbdbcbaabaababddcbdcdabddbbabbdddbdaaddacdabadcdbcbaadaddaadccadccdbaddcdccbccbdbabdadcdda");
    input.push_back("acacbbadcbbdabadbdacdadcbdbdbddcddbdbdbcbaaabaababdcbdcdaabddadbabacddbdbccddabdabacdcdcbcbaacdadacdcaaccdbddccbccbdabdadadcdda");
        Set_random(semillas[i]);

    MA _MA3(input, alphabet);
    solucion = _MA3.run();
    cout << "158_20 => Fitness: " << solucion.size() << endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    input.clear();
    // => 405_10:
    input.push_back("adadbbacdbbcaacdbccaacaacddbbbcbccdccbccdbcbdbaaabacabbdbcdcaabccacbababbdcccbcabaddccabcaabdadccdbdbaadcaccaadcdadacddbacdcdcddbddbcbabcacacdcaabbaddcddaabacbacbbddcddbcababcdbcacdbcaacadcdcbcccaaacccbaadbcdccabadbbaacdcaaacdbabcbcccbaabcdcddcaacaadacbcdabaadcacdcbbdbaabcabababaddbcbcacdadcdcbccacadaaacadddacdccdcacdaaccdcddcdadccccabdbccabbcbdcaaabacccbcbbacdacdacc");
    input.push_back("bdadbbabcaabacdbccddacaacddbcbcbccdcccbcbdbaaabaaabbcdbcdcaabccacbababdcccbcbaddcadbcaabdadcdcdbdbaadcaccaacddaddcbacdcdcddbddbcbabcaacdccaaabbaddcddaabacbacbbddcdacbaabcbccacdbacadcdcbccaaacccdbaadbcdccaadbbacdaacdbabccbccbaabcdcddcaacaadacadbcdabaadcacdcbbdbacbcabaabaaddcbcacdadcdcbbcadaaaadddacdcdacbcdaaccbdcddacdadccccadbabbcddaabaccbcbacdacdaccbc");
    input.push_back("badadbbacdbbcaabacdbccaddacaacddbbcbcbccdccbcdcbdaaaacaababccdbcdcabccacbababbdcccbcabadcadbcaabdadcdcdbdbaadcaccaadcadaddcbacdccddddbcbabcaacdcaaabbadcdaabcacbbddcdadbcabaacdbccacdbcaaadcdcbcccaabacccdcbaadcdccabadbbaccaaacdbabcbcccbaabcdacdcaacaadacadbcdabadcacdcbbdbaacabaabbddbcbcacdadcdbcbccadaaacdddacdccacbacdaccbcddacdadccccabdbccbcdbdcaaacccbcbbacdacdaccbc");
    input.push_back("badadbbdbbaabacdbccadaacddbbccbccdcbccdbcbdbaaabacababccdbcdcaabccacbababbdcccbcabaddccadbcaadadddbdbaadcaccaacadcddcacdcdcdbdbcbbcacacdccaabbadcddaabacbcbbddcdacbcabaabcdbccacdbaacaddcbcccaabacccdcbaadcdccababacdcaaacdbabccccbaabcdcddcaacaadacadbcdabaadcadcbdbacbcabaababddbcbcacdadcdcbcbcadaaaadddacdccdacbcdaaccbdcddacdadcccabdbccabdbdcaaacccbcbbacdacdaccb");
    input.push_back("badadbbacdbbcaaacdbccaddacaacddbbcbcbcdccbccbcbdbaaabacaababccdccabcccababbdcccbcabaddcadbcaabdadcdcdbdbaadcaccaadcdadadcbacdcdcddbddbbabacacccaaabaddcddaabacbacbbddcdacdbcabaacdbccacdbcacadcdcbcccaabacccdcbadbcdccabadbbaacdcaacdbabcccccbacdacddcaacadacadbcdabaadcacdcbbdbaacbcababbaddbcbcacdadcdcbcbcaadaaacaddddcdacbacdaaccbdcddacdadccccadbccabbcdbcaaacccbcbbacacdaccbc");
    input.push_back("badadbbacdbcaabacdbccaddacaacddbbcbcbccdccbccdcbdbaaabacaababccdccaabcccbababbdcccbcabadccabcaabdadcdcdbdbaadcacaadcdadacddcbacdcdcddbdbcbabcacacdccaaabbaddcdaabacbcbddcdacdbcabacdbccacdbcaacadcdccccaabacccdcbaadbcdcabadbbaacdcaaacdbabcccccbaabcdacddcaacaacadbcdabaadacdbbdbacbcabaababaddbcbcacdadcdcbcbcacadaaacadddacdccdacacdaccdcddcdcccabdbccabbcdbdcaaabacccbcbdacdaccbc");
    input.push_back("badadbacdbbaabacdcaddacaacddbbcbcbccdccbcdbbdbaaaacabbccdbcdcaabccacbababbdccccabaddccadbcabdadcdcddbaacaccadcdadaddcbacddcdbddcbacacacdccaabbaddcddabacbacbbdcdacbabaacdbcacdcaacacdcbcccaabacccdcbaadbcdccabadbbacdaaacdbabccbcccbaabcacdcacaadacadbabaadcacdcbbdbaacbcabaabaddcbcacdadcdcbcbcaadaaacadddacdcdacbacaaccbdcddacdadccccabdbcabbcdbdcaaabccccbbacacdaccbc");
    input.push_back("badadbbacdbbcaabacdbcaddacaacddbbcbcbccccbccdbcbdbaabacababccdbcdcaabcccbabbdcccbcbaddccadcaabdaddcdbaadcaccaadcdadacddcbacdcdcddbddbcbabcaacdccaaabbddcdaababacbbddcdadbcaaabcdbcccdcaacacdcbcccaabacccdcbaadbcdccabadbaacdcaadbaccbccbaabcdaddcaacaadacadbcdabaadcaccbbdbaacbcabaababaddcbcacdaddcbccaadaacadddaccdacbacdaacbdcddacdadccccbdbccabcdbdcaaabacccbcbbacdacdacbc");
    input.push_back("badadbbacdbbcaabacdbccaddacacddbbcbcbccdbccdbcbdbaaabacaabbccdbcdcaabccacbababbdcccbcabaddccadbcaabddcdcdbdbadcaccaadddacddacdcdddbdbcbabcacacdccaaabbddcdaabacbacbdcdacdbaacdcccdbcacadcdcbcccaabacccdcbaadbcdcabadbbaadcaaacdbabccbcccbaabcdcddcaacaadacadbcdabaadcacdbbdaacbcbaabaaddbcbcacdaddcbcbcacadaaacadddacdccdacbacdaaccbdcddcdadccadbccabbcdbdcaaabacccbbacdacdaccbc");
    input.push_back("bdadbacdbaabacdbccadacaacddbbcbcbccdccbcbdbaaabacaababccdbcdcaabccacbababbdcccbcaaddcadbcaabdadcdcddbadcaccaadddacdcbacdcdcddbddbcbabccacdccaaabbaddcddabacbacbbddcacdbabaabcdbccacdbcaacdcdcbccaabacdcbaadbcdccababaacdcaaacdbcbcccbaabcdacddcaacadacadcdabaadcacbbdbaacbbaaabaddbccacdadcdcbbcacadaacadddacdccdacacdaccbdcddacdadccccabdbccabbcdbdcaaabacccbcbacdacacbc");
        Set_random(semillas[i]);


    MA _MA4(input, alphabet);
    t0=clock();
    solucion = _MA4.run();
    t1=clock();
    time = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "405_10 => Fitness: " << solucion.size() << endl;
    cout << "Tiempo: " << time <<endl;
    

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    input.clear();
    // => 405_15:
    input.push_back("adadbbacdbbcaacbccacaacddbbbcbccdccbcdbcbdbaaabacabbdbcdcaabccacbaabbdcccbcabaddccabcadadccdbdbaadcaccaadcdadaddbaccdcddbddbcbabcacacdcaabbaddcdabacbcbbddcdbcbbcdbcacdbcaacadcdcbcccaaacccbaadbcdccabadbbaacdcaaacdbabcbcccaabcdcddcaaaadacbcdabaadcacdcbbdbabcababaaddbcbacdadcdcbcccadaaacadddacccdcadaacccddcdadccccabdbcabcbdcaaaaccbcbbacdacdacc");
    input.push_back("bdadabcaabacdbccddacaacddbcbcbcdcccbcbdbaabaaabbcdcdcaabccacbbabdcccbcbddcadbcaabdadcdcdbdbaadcaccaacddaddcbacdcdcddbddbcbabcaacdccaaabbaddcddaaacbacbddcdacbaacbccacdbacadcdcbccaaacccdbaadbcdccaadbbacdaacdbabccbccbaabcdcddcaaaadacadbcdabaadcacdcbbdbaccabaabaaddcbcacdadcdcbbcadaaaadddacdcdacbcdaaccbdcddacdadcccadbabbcdaabaccbcbadacdaccbc");
    input.push_back("bdadbbacdbbcaabacdbccaddacaacddbbcbcbccdccbcdcbdaaacaababccdbcdcabcacbababbdccbcbadcadcabdadcdcdbdbaadcaccaacadaddcbacdccddddbcbabcaacdcaaabbadcdaabcacbbddcdaabaacdbccacdbcaaadcdcbcccaabaccdcbaadcdccbadbbaccaaacdbabcbcccbaabcdacdcacaadcadbcdabacacdcbbdbacabaabbddbcbcacdadcdbcbccadaaacdddacdccacbacdaccbcddacdadccccabdbccbcdbcaaacccbcbbacdacdaccbc");
    input.push_back("baddbbdbaabacdbccadaacddbbcbccdbccdbcbdbaabacababccdbcdcabccacbababbdcccbcabaddccadbcaadadddbdbaadcaccaacadcddcacdcdcdbdbcbbacacccaabbadcddaabacbcbbddcdacbcabaabcdbccacdbacaddcbcccaabacccdcbadcdccababacdcaaacdbabccccbaabcdddcaacaadacadbcdabaadcadcbdbaccabaaabddcbcacdadcdcbcbcadaaaadddacdccdacbcdaaccbdcddacdadcccabdbccabdbdcaacccbbbacdacdccb");
    input.push_back("badadbbacdbbcaadbccaddacaacddbbcbcbcdccbccbcbbaaabacaababccdcabcccaabbdccbcabaddcadcaabdadcdcbbaadcaccaadcdadacbacddcddbdbbabacacccaaabaddcddaabacbacbbdddbcabaacdbccacdbcacadcdcbccaabacccdcbadbcdccabadbbaacdcaadbabcccccbacdacddcaaadacadbcdaaadcacdcbbdbaaccababbaddbcbcacdadcdcbcbcaadaaacaddddcacbacdaaccbdcddacdadcccadbccabcdbcaacccbbbacacdaccbc");
    input.push_back("badadbbacdbcaabcdbccaddacaacddbbcbcbccdccbccbdbaaabaaabaccdccaabcccbababbdcccbcabadccabcaabdadcdcdbdbaadcacaadcdadacddcbacdcdcddbdbcbabcacacdccaabbaddcdaabaccbdcdacdbcabacdbcccdbcaacaddcccaabacccdcbaabcdabadbbaacdcaaacdbabcccccbaabcdacddcaacaacadbcdabadacdbbdbacbcabaababaddbcbcacdadcdcbcbcacadaaacaddacdccdacacdacccddcdcccabbccabbcdbdcaaabacccbcbdacdaccbc");
    input.push_back("adadbadbbaabacdcadacaacddbbcbcbccdccbcdbbdbaaacabbccdbcdcaabccacbbabbdccabaddccadbcabdadcdcddbaacaccaddadaddcbacddcbddcbacaacdccaabbaddcddabacbacbbdcdacbabaacdbcacdaacacdcbcccaabacccdcbaadbcdccabadbbacdaaacdbabccbcccbaabcacdcacadacadbabaadcacdcbbdbaacbcababddcbcacdadcdcbcbcaadaaacadddacdcdacbacaaccbdcddacdadcccabdbcabbcdbdcaaabcccbbacacdaccbc");
    input.push_back("badadbbacdbbcaabacdbcaddacaacddbbcbcbcccbccdbcbdbaacababcdbcdcaabcccbabbdcccbcbaddccadabdadddbaadcaccaadcadacddcbacdcdcddbddbcbabcacdccaaabbddcdaababacbbddcdadbcaaabcdbcccdcaacacdcbcccaabacccdcbaadbcdccabadbaacdaadbaccbccbaabdaddcaacadacadbcdabaadcaccbbdbaacbcabaababaddcbcacdadcbcaadaacaddaccdacbcdaacbdcddacdadccccbdbccabcdbdcaaabacccbcbacdacdacbc");
    input.push_back("badadbbacdbbcabacdbccaddacacddbbcbcbccdbccdbcbdbaaabacaabbcdcabccacbababdccccbaddccdbcaabddcdcdbdbacaccaaddacddacdcddbdbcbabcacacdccaaabbdddaabacacbdcdacdbaacdcccdbcacadcdcbcccaabacccdcbadbcdcabadbbaadcaacdbabccbcccbaabcdcddcaacaadacadbcdabaadcacdbbdaacbcbaabaaddbcbcacdaddcbccaadaaacadddacdccdacbacdaaccbdcddcdadccadbccabbcdbdcaaabacccbbacdacdaccbc");
    input.push_back("bdadbacdbaabacdbcadacaacddbbbcbccdccbcbdbaaabacaababcdbcdcaabccacbababbdcccbaaddcadbcaabdadcdcddbadcaccaadddacdcbacdcdcddbddbcbabcacdcaaabaddcddabacbacbbddcacdbabaabcdbccacdbcaacdcdccaabacdcbaadbcdccaabaacdcaaacdbcbcccbaabcdacddcaacadacadcdabaadcacbdbaacbbaaabaddbccacdadcdcbbccadaacadddacdccacacdaccbdcddacdadcccabdbcabbcdbdcaaabacccbcbacdacacbc");
        Set_random(semillas[i]);

    MA _MA5(input, alphabet);
    solucion = _MA5.run();
    cout << "405_15 => Fitness: " << solucion.size() << endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    input.clear();
    // => 405_20:
    input.push_back("aadbbacdbcaacccacaacdbbbcbccdccbcdbcbdbaaabacabbdbcdcaabccacbaabbdccbcabaddcabcadadccddbacacaadcdadaddbaccdcddbddbcbabccacdcaabadddabacbcbbddcdbcbbcdbcacdbcaacdcdcccaaacccbaadbcdccabadbbaadcaacdbabcbcccaabcdcddcaaadcbcdabaadacdcbbdbabcababaaddbcbacdadcdcbcccadaaacadddcccdcadaacccdddadccccabdbcabcbdcaaaaccbcbbacdacdacc");
    input.push_back("bdadabcaabacdbccddacaacddbcbcbcdcccbcbdbaabaaabbcdcdcaabcacbbabcccbcbddcabaabadcdcdbbaadcaccaacdacbacdcdcdbddbbabcaacdccaaabbaddcddaaacbacbddcdacbaacbcacdbacadcdcbccaacccdbaadbcdccaadbbacdaacdbabccbccbaabcdcddcaaaadcadbdabaadacdcbdbaccababaadcbcacdadcdcbbadaaaadddacdcdacbcdaaccbdcddacdadcccdbabbdaabaccbcbadacdaccbc");
    input.push_back("bdabbacdbbcaabacdbccaddacaacddbbcbcbcdccbcdcbdaacaababccbcdcabcacbababbdccbcbadcdcabdadcdcdbdbaccaaadaddcbacdccddddbcbabaacdcaaabbadcdaacacbbddcdaabaacdbccacdbcaacdbcccaabaccdcaadcdccbadbbaccaacdbabcbcccbaabcdacdcacaadcadbcdabacacdcbbdbacabaabbddbcbcacdadcdbcbccadaaacdddacdccacbacdaccbcddacdadccccabbccbcdcaaacccbcbbacdacdaccbc");
    input.push_back("baddbdbaabacdbccadaacdbcbcdccdbcbdbabacababccdbcdcaccacbababbdcccbcabadccadbcaadadddbdbaadcacaacadcddcacdcdcddbcbbaccccaabbadcddaabcbcbbddcdacbcabaabcdbccacdbacaddcbcccaabaccdcbadcdcbabacdcaaacdbabccccbaabcdddcaacadacadbcdabaadcacbdbaccabaaabddcbcacdadcdcbcbcadaaaaddaccdabcdaacbdcddacdadcccabdbccabdbdcaacccbbacdacdccb");
    input.push_back("bdbbacdbbcaadbccaddacaacddbbcbcbcdccbccbcbbaabacaaabcdcabcccaabbdccbcabaddcadcaabdadcdcbbaadcaccaadcdadcbacddcddbdbbabacaccaaabaddcddaabacbacbbdddbcabaacdbccacdbcacadcdcbcaaacccdcbadbcdcabadbbaacdcaadbabcccccbacdacddcaaadcadbcdaaadcacdcbbdbaaccababbddbcbcacdaddbcbcaadaaacaddddcacbacdaaccbdcddacdadcccadbccabcdbcaacccbbbacacdacbc");
    input.push_back("bdadbbacdbcaabcdbccaddacaacddbbcbcbccdccbccbbaaabaaabaccdccaabccbababbdcccbcabadccabcaabdadcdcdbdbaadcacaadcadacddcbacdcdcddbdbcbabcacacdccaabbadcdaabaccbdcdacdbcabacdbcccbcaacaddcccaabacccdcbabcdabadbbaacdcaacdbabcccccbaacdacdcaacaacadbcdabadacdbbdbacbcabaaabaddbcbacdadcdcbcbcacadaaacadacdccdacacdacccddcdcccabbccabbcdbdcaaabaccbcbdacdaccbc");
    input.push_back("adadbadbbaabacdcadacaacddbbcbcbccdcbcdbdbaaacabbccdbcdcaabccacbbabbdccabadccababdadcdcddaacaccadddaddcbacddcbddcbacaacdcaabbaddcddabacbacbbdcdacbabaacdbcacdaacaccbccaabacccdcaadbcdccabdbbacdaaacdbabccbcccbaacacdcacadacadbabaadcacdcbbbaacbcababdcbcacdaddcbcbcaadaaacadddcdcdcbacaaccbdcddacdadccabdbcabbcdbdcaaabcccbbacacdaccbc");
    input.push_back("badadbbacdbbcaabacdbcaddacaacddbbcbbcccbccdbcbdbcababcbcdcaabcccabbdcccbcbdccadabddddbaadcaccaadcadacddcacdccddbddbcbabcacdccaaabbddcdaababcbbddcdadbcaaabcdbcdcaacacdcccaabccdcbaadbcdccabadbaacdaadbaccbccbabdaddcaacadacadbcdabaadcaccbbdbaacbcabababaddcbcacdadcbcadaacaddaccdacbcdaacbdcddacdadccccbdbccabcdbdcaaabacccbcbacdacdacbc");
    input.push_back("babbadbbcabacdbccaddacadbbcbcbccdbccdbcbdbaaabacaabbcdcbccacbababdccccbadccdbcaabddcdcdbdbacaccaddaddacdcddbdbcbabcacacdccaabdddaabacacbdcdacdbaacdcccdbcacadcdcbccabaccdcadcdcabadbbaadcaacdbabcccccbaabccddcaacaadacadbcdabaacacdbbdaacbcbaabaaddbcbcacdaddcbccaadaaacadddacdcccbacdaaccbdcddcdadccadbccabbcdbdaaabacccbbacdacdaccb");
    input.push_back("bdadbacdbaabacdcadacaacddbbcbccdccbcbdbaaabaababcdbcdcaabccacbababbdcccaadadbcaabdadcdcddbadcacaadddacdcbacdccddbddbcbabcacdcaaabddcddbabacbbddccdbabaabcdbccacbcaacdcdccaabacdcaaddccaabaaccaaacdbcbcccbaabcdacddcaaaacadcdabaadcacbdbacbbaaabaddbacdadcdcbbccaaacdddacdccaccdaccbdcdaadccabbcabbcdbdcaaabacccbcbacdacacbc");
            Set_random(semillas[i]);

    MA _MA6(input, alphabet);
    solucion = _MA6.run();
    cout << "405_20 => Fitness: " << solucion.size() << endl;


}


}