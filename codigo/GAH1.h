#ifndef GAH1_H
#define GAH1_H

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cmath>
#include <random>
//#include <cstdlib>

using namespace std;

void Set_random (unsigned long x);
float Rand(void);
int Randint(int low, int high);

class GAH1{

    private:
        vector<string> input;
        int sumInput;
        vector<vector<vector<float>>> pop_aux;
        vector<multimap<int,vector<float>>> pop;
        vector<vector<int>> eval;
        string alphabet;
        float Pcross;
        float Pmut;
        

    public:
        GAH1(vector<string> input, string alphabet);
        void normalize_sum_1(vector<float>& chr);
        void initialize_pop();
        int evaluate_chr(vector<float> chr);
        void evaluate_pop();
        void select(vector<float>& p, int subpopIndex);
        vector<float> cross_over(vector<float> p1, vector<float> p2);
        void mutate(vector<float>& child);
        void replacement(vector<float> child, int subpopIndex);
        string get_sequence(vector<float> chr);
        string best_sequence();
        string run();


};

#endif