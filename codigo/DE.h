#ifndef DE_H
#define DE_H

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cmath>
#include <random>
//#include <cstdlib>

using namespace std;

void Set_random (unsigned long x);
float Rand(void);
int Randint(int low, int high);

class DE{

    private:
        vector<string> input;
        int sumInput;
        vector<vector<float>> pop;
        //multimap<int,vector<float>> pop;
        vector<int> eval;
        string alphabet;
        float Pcross;
        float Pmut;
        float F;
        int best_eval;
        

    public:
        DE(vector<string> input, string alphabet);
        void normalize_sum_1(vector<float>& chr);
        void initialize_pop();
        int evaluate_chr(vector<float> chr);
        void evaluate_pop();
        vector<float> new_sample(vector<float> P);
        vector<float> best_of_pop();

        string get_sequence(vector<float> chr);
        string best_sequence();
        string run();


};

#endif