#ifndef H1_H
#define H1_H

#include<iostream>
#include<vector>
#include<map>
#include<set>
#include <algorithm>
#include <functional>

using namespace std;

void Set_random (unsigned long x);
float Rand(void);
int Randint(int low, int high);

class H1{
    
    private:
        vector<string> input;
        string alphabet;
        map<char,int> pesos;

    public:
        H1(vector<string> input, string alphabet);
        string run();

};


#endif