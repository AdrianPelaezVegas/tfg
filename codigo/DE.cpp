#include "DE.h"

//////////////////////////////////////////////////////////////////////////////////////////

//Metodos random:

#define MASK 2147483647
#define PRIME 65539
#define SCALE 0.4656612875e-9

unsigned long Seed = 0L;
void Set_random (unsigned long x){
    Seed = (unsigned long) x;
}
float Rand(void){
    return (( Seed = ( (Seed * PRIME) & MASK) ) * SCALE );
}
int Randint(int low, int high){
    return (int) (low + (high-(low)+1) * Rand());
}
float Randfloat(float low, float high){
    return (low + (high-(low))*Rand());
}

//////////////////////////////////////////////////////////////////////////////////////////

DE::DE(vector<string> input, string alphabet){
    this->input = input;
    this->sumInput = 0;
    for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++)
        this->sumInput += it->size();
    this->alphabet = alphabet;
    this->Pcross = 0.5;
    this->F = 0.5;
    this->Pmut = 0.02;
}

//////////////////////////////////////////////////////////////////////////////////////////

void DE::normalize_sum_1(vector<float>& chr){
    //Calculamos la suma de todos los valores del cromosoma:
    float sum = 0;
    for(auto it = chr.begin(); it != chr.end(); it++)
        sum += *it;
    //Dividimos cada gen por este valor:
    for(int i=0; i<chr.size(); i++)
        chr[i] = chr[i]/sum;
}

//////////////////////////////////////////////////////////////////////////////////////////

void DE::initialize_pop(){
    vector<float> chromosome;

    //multimap<int, vector<float> , greater<int>> subpop;

    int nChr = 50;

    for(int j=0; j<nChr; j++){            
        // Cromosoma random:
        for(int k=0; k<this->sumInput; k++){
            chromosome.push_back(Rand());
        }            
        // Normalizamos cromosoma sum 1:
        this->normalize_sum_1(chromosome);
        // Añadimos cromosoma a la subpop, con eval=0 provisional, hasta que en la siguiente fase se calculen las eval:
        //pair<int , vector<float>> aux (0,chromosome);
        //subpop.insert(aux); 
        this->pop.push_back(chromosome);           
        chromosome.clear();
    }        
}

//////////////////////////////////////////////////////////////////////////////////////////

int DE::evaluate_chr(vector<float> chr){

    // SE EJECUTARA LA HEURISTICA H1 MODIFICADA:
    // Representación de los parametros en forma de matriz:
    vector<vector<float>> params;
    vector<string> in = this->input;
    int pos_chr = 0;
    for(int i=0; i<in.size(); i++){
        vector<float> aux;
        for(int j=0; j<in[i].size(); j++){
            aux.push_back(chr[pos_chr]);
            pos_chr++;
        }
        params.push_back(aux);
    }


    string solucion;

    map<char,float> pesos;

    for(auto it = this->alphabet.begin(); it != this->alphabet.end(); it++){
        pesos[*it] = 0;
    }
    typedef function<bool(pair<char, float>, pair<char, float>)> Comparator;
 
	Comparator compFunctor = [](pair<char, float> elem1 ,pair<char, float> elem2){
		return elem1.second > elem2.second;
	};
    

    bool isEmpty = false;

    //H1:
    while(!isEmpty){
        
        //Contamos los pesos:
        for(int i=0; i<in.size(); i++){
            if(!(in[i].empty()))
                pesos[in[i][0]] = pesos[in[i][0]] + (in[i].size() * params[i][0]);
                //pesos[in[i][0]] = pesos[in[i][0]] + params[i][0];
        }
       
        //Creamos un multiset ordenado con los valores del map frecuencias, de esta forma el primer elemento del multiset
        // (*.begin()) será el caracter con mayor frecuencia:
        multiset<pair<char, float>, Comparator> pesosOrdenados(pesos.begin(), pesos.end(), compFunctor);
        
        //Seleccionamos el caracter con mayor frecuencia:
        char masPeso = pesosOrdenados.begin()->first;

        //Lo añadimos al superstring:
        solucion.push_back(masPeso);
        //cout << "------->"<< masPeso << endl;
        //Lo eliminamos del front de las cadenas de entrada:
        for(int i=0; i<in.size(); i++){
            
            if(!in[i].empty()){ 
                if(in[i][0] == masPeso){
                    /*
                    cout << "CAracter del input: " << in[i][0] << endl;
                    cout << "Mas peso: " << masPeso << endl;
                    cout << "Parametro: " << params[i][0] << endl; 
                    */
                    in[i].erase(in[i].begin());
                    params[i].erase(params[i].begin());                
                }
            }
        }
                      

        //Comprobamos que todavía quedan carecteres en alguna cadena:    
        isEmpty = true;
        for(vector<string>::iterator it = in.begin(); it != in.end(); it++)
            if(!(it->empty()))
                isEmpty = false;

        //Limpiamos las estructuras de datos necesarias:

        for(string::iterator it = this->alphabet.begin(); it != this->alphabet.end(); it++){
            pesos[*it] = 0;
        }
    }
    return solucion.size();
}

//////////////////////////////////////////////////////////////////////////////////////////

void DE::evaluate_pop(){
   this->eval.clear();
    for(int j=0; j<this->pop.size(); j++){
        
        this->eval.push_back(this->evaluate_chr(this->pop[j]));
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
vector<float> DE::new_sample(vector<float> P){
    //Select 3 cromosomas diferentes de pop:
    int i1 = Randint(0,this->pop.size()-1);
    vector<float> P1 = this->pop[i1];
    int i2 = i1;
    while (i1 == i2){
        i2 = Randint(0,this->pop.size()-1);
    }
    vector<float> P2 = this->pop[i2];
    int i3 = i2;
    while (i3 == i2 || i3 == i1){
        i3 = Randint(0,this->pop.size()-1);
    }
    vector<float> P3 = this->pop[i3];

    vector<float> ret;

    for(int i=0; i<P1.size(); i++){
        if(Rand() < this->Pcross){
            float aux = P1[i]+this->F*(P2[i]-P3[i]);
            //--->Cuidado que no salgan negativos:
            if(aux < 0) aux = 0.00000001;
            ret.push_back(aux);
        }
        else{
            ret.push_back(P[i]);
        }
    }

    return ret;
}

//////////////////////////////////////////////////////////////////////////////////////////

string DE::get_sequence(vector<float> chr){
    // SE EJECUTARA LA HEURISTICA H1 MODIFICADA:

    // Representación de los parametros en forma de matriz:
    vector<vector<float>> params;
    
    int pos_chr = 0;
    for(int i=0; i<input.size(); i++){
        vector<float> aux;
        for(int j=0; j<input[i].size(); j++){
            aux.push_back(chr[pos_chr]);
            pos_chr++;
        }
        params.push_back(aux);
    }


    string solucion;

    map<char,float> pesos;

    for(auto it = this->alphabet.begin(); it != this->alphabet.end(); it++){
        pesos[*it] = 0;
    }

    typedef function<bool(pair<char, float>, pair<char, float>)> Comparator;
 
	Comparator compFunctor = [](pair<char, float> elem1 ,pair<char, float> elem2){
		return elem1.second > elem2.second;
	};
    

    bool isEmpty = false;

    //H1:
    while(!isEmpty){
        
        //Contamos los pesos:
        for(int i=0; i<this->input.size(); i++){
            if(!(this->input.empty()))
                pesos[this->input[i][0]] = pesos[this->input[i][0]] + (input[i].size() * params[i][0]);
        }
        
        //Creamos un multiset ordenado con los valores del map frecuencias, de esta forma el primer elemento del multiset
        // (*.begin()) será el caracter con mayor frecuencia:
        multiset<pair<char, float>, Comparator> pesosOrdenados(pesos.begin(), pesos.end(), compFunctor);
        
        //Seleccionamos el caracter con mayor frecuencia:
        char masPeso = pesosOrdenados.begin()->first;

        //Lo añadimos al superstring:
        solucion.push_back(masPeso);

        //Lo eliminamos del front de las cadenas de entrada:
        for(int i=0; i<this->input.size(); i++){
            if(input[i][0] == masPeso){
                input[i].erase(input[i].begin());
                params[i].erase(params[i].begin());                
            }
        }
        //Comprobamos que todavía quedan carecteres en alguna cadena:    
        isEmpty = true;
        for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++)
            if(!(it->empty()))
                isEmpty = false;

        //Limpiamos las estructuras de datos necesarias:

        for(string::iterator it = this->alphabet.begin(); it != this->alphabet.end(); it++){
            pesos[*it] = 0;
        }
        
    }
    return solucion;
}

//////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////

vector<float> DE::best_of_pop(){
    int min = 999999;
    vector<float> ret;
    for(int i=0; i<this->eval.size(); i++){
        if(eval[i] < min){
            min = eval[i];
            ret = this->pop[i];
            this->best_eval = eval[i];
        }
    }
    return ret;
}
//////////////////////////////////////////////////////////////////////////////////////////

string DE::run(){
    int stop = 0;
    int control =0;
    int count_best=0;
    vector<float> best;
     unsigned t0,t1;
    double time;
    t0 = clock();
    this->initialize_pop();
    this->evaluate_pop();
    while(stop < 150000){
        vector<vector<float>> newPop;
        for(int i=0; i<this->pop.size();i++){
            vector<float> x = this->new_sample(this->pop[i]);
            
            int cost_new = this->evaluate_chr(x);
          
            if(cost_new < this->eval[i]){
                newPop.push_back(x);
                this->eval[i] = cost_new;
            }
            else{
                newPop.push_back(this->pop[i]);
            }
            
            stop++;
          
        }
        this->pop = newPop;

        //int aux = this->best_eval;

        best = this->best_of_pop();

       /* if (aux == this->best_eval){
            count_best++;
            if(count_best == 1000){
                stop = 150000;
            }    
        }
        else{
                count_best=0;
        }
        */
        //cout << this->best_eval << endl;
        //cout << "a";

        

        if(control%100 == 0){
            int x = this->best_eval;
            t1 = clock();
            time = (double(t1-t0)/CLOCKS_PER_SEC);
            cout << time << " " << x << endl;
        }
        control++;
        
    }
    return this->get_sequence(best);
}

//////////////////////////////////////////////////////////////////////////////////////////