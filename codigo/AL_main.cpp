#include "AL.h"
#include<ctime>
#define MASK 2147483647
    #define PRIME 65539
    #define SCALE 0.4656612875e-9

    unsigned long Seed = 0L;
    void Set_random (unsigned long x){
        Seed = (unsigned long) x;
    }
    float Rand(void){
        return (( Seed = ( (Seed * PRIME) & MASK) ) * SCALE );
    }
    int Randint(int low, int high){
        return (int) (low + (high-(low)+1) * Rand());
    }
    float Randfloat(float low, float high){
        return (low + (high-(low))*Rand());
    }

  
int main(){
    unsigned t0,t1;
    double time;
 string solucion;
    string alphabet = "abcd";
    vector<string> input;

    vector<int> semillas;
    semillas.push_back(534);
    semillas.push_back(9834);
    semillas.push_back(3);
    semillas.push_back(155);
    semillas.push_back(846);
    semillas.push_back(980);
    semillas.push_back(34527);
    semillas.push_back(57);
    semillas.push_back(34);
    semillas.push_back(6345);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ALPHABET-LEFTMOST
cout << "=============== ALPHABET-LEFTMOST ====================" << endl;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
for(int i=0; i<10; i++){
    // => 158_10:
    input.clear();
    input.push_back("acacbbadcbbdaadcbddaadaadccbbbdbddcddbddcbdbcbaaabadabbcbdcdaabddadbababbcdddbdabaccddabdaabcacddcbcbaacdaddaacdcacadccbadcdcdccbccbdbabdadadcda");
    input.push_back("bcacbbadcbbdaabadcbddacdaadcbbdbdddcddbddcbdbcbaaabadaabdcbdcdaabddadbababbcdddbdabacddacbdaabcadcdcbcbaacdaaacdcacadccdbadcdcdcbccdbabdadadcdda");
    input.push_back("bacacbbacbbdaabadcbddaccadadcbbdbdbdcddddcbdbcbaaabadaabaddcbdcdaabddadbababbcdddaaccddaaabcacdcdcbbaacdaddaadcacadcdaddcdccbccbdabddacdda");
    input.push_back("bacacbbadcbbaabadcbdaccaaadccbbdbdbddcddbddcbdbcbaabdababddcbdcdaabddadbababcdddbdabaccddacbdaabcacdcdcbcbaadaacdacadccdacdcdccbccdbbdadacdda");
    input.push_back("bacacbadcbbaadcbddaccadadccbbdbbddcddbddcbdbcbaaabadaababddcbddaabddadbaabbdddbdabaccddcbabcaccdcbcbacdaddacdcacadccdbadcdcdccbcbabdadddd");
    input.push_back("baccbbacbbdaabadcbddaccadaadccbbdbdbddcddbddcbdbcbaaabadaababddbcdabdddbababbcdddbdaaccddacbdaabcacdcdcbcaadaddaacdcacadccdbadcdcdccbccdbadadadcdd");
    input.push_back("baccbbadcbbdabacbddaccaadccbddbddcddbdcbdbcbaabadaababddcdcdaabddadbababbcdddbdbaccddacbdabacdcdcbcbacdaddaacdcacaccdbadcdcdccbccbbabdadadcdda");
    input.push_back("bacbbadbdaabadcbddaccdaadcbbdbdbdcddbddcdbcbaaaadababddcbdcdaabddadbbbbcdddbbaccddacbdaabcacdcdcbcbaacddacdcacadccdbdcdccbccdbabdaadcdda");
    input.push_back("bacacbbadbbdaabadcbddaccadaadccbbdbdbddcddbddcbdbcbaabaababddcbdcdaabddbbabbdddbdaacddacdaabcacdcdbcbaadaddaacdccadccdbaddcdccbccbdbabdaddcdda");
    input.push_back("acacbbadcbbdaabadcbdaccadadcbdbdbddcddbdbdbcbaaabdaababddcbdcdaabddadbababcddbdbaccddacbdabcacdcdcbcbaacdadacdcaadccdbadcdcdcbccbdbabdadadcdda");
        Set_random(semillas[i]);

      AL _AL1(input, alphabet);
    t0=clock();
    solucion = _AL1.run();
    t1=clock();
    time = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "405_10 => Fitness: " << solucion.size() << endl;
    cout << "Tiempo: " << time <<endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    input.clear();
    // => 158_15:
    input.push_back("acacbbadcbbdaadbddadaadccbbbdbddcddbdcbdbcbaaabadabbcbdcdaabddadbaabbcdddbdabaccddabdacacddcbcbaacdaddaacdcacaccbaddcdccbccbdbabdadadcda");
    input.push_back("bcacbbadbdaabdcbddadaacbdbdddcddbddcbdbcbaaabadaabdcbdcdaabddadbababbcdddbdabacddacbaabcadcdcbcaacdaaacdcacadccdbadcdccbccdbadadadcda");
    input.push_back("bacacbbacbdaabadcbddacadadcbdbdbcddddcbdbcbaaabdaaaddcbdcabddadbababbcdddaaccaaabcacdcdcbbaacdaddaadcacdcdaddcdccbcbdabddacda");
    input.push_back("bacacbbadbbaabadcbdccaaadccbbdbdbddcddbddcbdbcbaabdababddcbdcdaabddadbababcdddbdabaccddacbdabcacdccbcbaadaadacadccdacdcdccbccdbbdadacdda");
    input.push_back("bacacbadcbbaadcbddaccadadccbbdbbddcddddcbdbcbaaabadaababddcbddabddadbaabbdddbdabaccddcbabcaccdcbcbacdaddacdcacadccdbadcddccbcbabdaddd");
    input.push_back("baccbbacbdaabadcbdaccadaadccbbdbdbddcddbddcbdbcbaaabadaababddbcdabddbababbcdddbdaacddacbdaabcadcdbcaadadacdcacadccdbadcdcdccbcdbadadadcdd");
    input.push_back("baccbbadcbbdabacbddaccaadccbddbddcddbdbcbaabadaababddcdcdaabddadbababcdddbdbaccdacbdabacdcdcbcbacdaddaacdcacaccbadccdccbccbbbdadadcdda");
    input.push_back("acbbadbdaabadcbddaccdaadcbbdbdbdcddbddcdbcbaaaadababddcbdcdaabddadbbbbcddbbaccddacbdaabcacdcdcbcaacddcdcacadccdbdcdccbcdbabdadcdda");
    input.push_back("bacabbadbbdaabadcbddccadaadccbbdbdbddcddbddcbdbcbaabaababddcbdcdaabddbbabbdddbdaacddacdabcadcdbcbaadaddaacdccadccdbaddcdccbccbdbabdadcdda");
    input.push_back("acacbbadcbbdaabadbdaccadadcbdbdbddcddbdbdbcbaaabaababddcbdcdaabddadbababcddbdbccddabdabacdcdcbcbaacdadacdcaadccdbadcdcdcbccbdbabdadadcdda");
        Set_random(semillas[i]);

       AL _AL2(input, alphabet);
    t0=clock();
    solucion = _AL2.run();
    t1=clock();
    time = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "158_15 => Fitness: " << solucion.size() << endl;
    cout << "Tiempo: " << time <<endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    input.clear();
    // => 158_20:
    input.push_back("aacbbadcbdaadddadaadcbbbdbddcddbdcbdbcbaaabadabbcbdcdaabddadbaabbcddbdabaccdabdacacddccbadadaacdcacaccbaddcdccbccbdbabddadcda");
    input.push_back("bccbbdbdaabdcbddadaacbdbdddcddbddcdbcbabadaabdcbdcdaabddadbababcddddabacddacbaabcadcdcbaacaaacdcacaccdbadcdccbccdbadadadcda");
    input.push_back("bacacbbacbdaabadcbddcadadcbdbdbcdddcbdbcbaaabdaaaddcbdcabddadbababbcdddaaccaaabcacdcdcbbaacdaddaadcacdcdaddcdccbcbdabddacda");
    input.push_back("bacacbadbbabadcbdccaadcbbbdbddcdbddcbdbcbaadabddcbdcdabddabababcdddbdabaccddacbdabcacdccbcbaadaadacdccdacdcdccbccdbdadacdda");
    input.push_back("bacacbadcbbaadcbddaccadadccbbdbbddcddddcdbcbaabadababddbddabddabaabdddbdabaccddcabcaccdcbcbacdaddacdcacadccdbadcddcccbabdddd");
    input.push_back("baccbbacbdaabadcbdacadaadccbbdbdbddcddbddcbdbcbaaabadababddbcdabdbababbcddbdaacddacbdaabcadcdbcaaadacdcacadccdbdddcbdbadadadcdd");
    input.push_back("baccbbadcbdabacbddaccaadccddbddcddbdbcbaabadaababddcdabddadbababcddbdbaccdacbdabaccdcbcbacdaddaacdcacaccbadccdccbccbbbdadadcdda");
    input.push_back("acbbadbdaabadcbddaccdaadcbbdbdbdcddbddcdbcbaaaadababddcbdcdaabdadbbbbddbbaccddacbdaabcacdcdcbcaaddcdcacadccdbdcdccdbaddcdda");
    input.push_back("bacabadbbdaabadcbddcadaadccbbdbdbddcddddcbdbcbaabaababddcbdcdabddbbabbdddbdaaddacdabadcdbcbaadaddaadccadccdbaddcdccbccbdbabdadcdda");
    input.push_back("acacbbadcbbdabadbdacdadcbdbdbddcddbdbdbcbaaabaababdcbdcdaabddadbabacddbdbccddabdabacdcdcbcbaacdadacdcaaccdbddccbccbdabdadadcdda");
        Set_random(semillas[i]);

       AL _AL3(input, alphabet);
    t0=clock();
    solucion = _AL3.run();
    t1=clock();
    time = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "158_20 => Fitness: " << solucion.size() << endl;
    cout << "Tiempo: " << time <<endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    input.clear();
    // => 405_10:
    input.push_back("adadbbacdbbcaacdbccaacaacddbbbcbccdccbccdbcbdbaaabacabbdbcdcaabccacbababbdcccbcabaddccabcaabdadccdbdbaadcaccaadcdadacddbacdcdcddbddbcbabcacacdcaabbaddcddaabacbacbbddcddbcababcdbcacdbcaacadcdcbcccaaacccbaadbcdccabadbbaacdcaaacdbabcbcccbaabcdcddcaacaadacbcdabaadcacdcbbdbaabcabababaddbcbcacdadcdcbccacadaaacadddacdccdcacdaaccdcddcdadccccabdbccabbcbdcaaabacccbcbbacdacdacc");
    input.push_back("bdadbbabcaabacdbccddacaacddbcbcbccdcccbcbdbaaabaaabbcdbcdcaabccacbababdcccbcbaddcadbcaabdadcdcdbdbaadcaccaacddaddcbacdcdcddbddbcbabcaacdccaaabbaddcddaabacbacbbddcdacbaabcbccacdbacadcdcbccaaacccdbaadbcdccaadbbacdaacdbabccbccbaabcdcddcaacaadacadbcdabaadcacdcbbdbacbcabaabaaddcbcacdadcdcbbcadaaaadddacdcdacbcdaaccbdcddacdadccccadbabbcddaabaccbcbacdacdaccbc");
    input.push_back("badadbbacdbbcaabacdbccaddacaacddbbcbcbccdccbcdcbdaaaacaababccdbcdcabccacbababbdcccbcabadcadbcaabdadcdcdbdbaadcaccaadcadaddcbacdccddddbcbabcaacdcaaabbadcdaabcacbbddcdadbcabaacdbccacdbcaaadcdcbcccaabacccdcbaadcdccabadbbaccaaacdbabcbcccbaabcdacdcaacaadacadbcdabadcacdcbbdbaacabaabbddbcbcacdadcdbcbccadaaacdddacdccacbacdaccbcddacdadccccabdbccbcdbdcaaacccbcbbacdacdaccbc");
    input.push_back("badadbbdbbaabacdbccadaacddbbccbccdcbccdbcbdbaaabacababccdbcdcaabccacbababbdcccbcabaddccadbcaadadddbdbaadcaccaacadcddcacdcdcdbdbcbbcacacdccaabbadcddaabacbcbbddcdacbcabaabcdbccacdbaacaddcbcccaabacccdcbaadcdccababacdcaaacdbabccccbaabcdcddcaacaadacadbcdabaadcadcbdbacbcabaababddbcbcacdadcdcbcbcadaaaadddacdccdacbcdaaccbdcddacdadcccabdbccabdbdcaaacccbcbbacdacdaccb");
    input.push_back("badadbbacdbbcaaacdbccaddacaacddbbcbcbcdccbccbcbdbaaabacaababccdccabcccababbdcccbcabaddcadbcaabdadcdcdbdbaadcaccaadcdadadcbacdcdcddbddbbabacacccaaabaddcddaabacbacbbddcdacdbcabaacdbccacdbcacadcdcbcccaabacccdcbadbcdccabadbbaacdcaacdbabcccccbacdacddcaacadacadbcdabaadcacdcbbdbaacbcababbaddbcbcacdadcdcbcbcaadaaacaddddcdacbacdaaccbdcddacdadccccadbccabbcdbcaaacccbcbbacacdaccbc");
    input.push_back("badadbbacdbcaabacdbccaddacaacddbbcbcbccdccbccdcbdbaaabacaababccdccaabcccbababbdcccbcabadccabcaabdadcdcdbdbaadcacaadcdadacddcbacdcdcddbdbcbabcacacdccaaabbaddcdaabacbcbddcdacdbcabacdbccacdbcaacadcdccccaabacccdcbaadbcdcabadbbaacdcaaacdbabcccccbaabcdacddcaacaacadbcdabaadacdbbdbacbcabaababaddbcbcacdadcdcbcbcacadaaacadddacdccdacacdaccdcddcdcccabdbccabbcdbdcaaabacccbcbdacdaccbc");
    input.push_back("badadbacdbbaabacdcaddacaacddbbcbcbccdccbcdbbdbaaaacabbccdbcdcaabccacbababbdccccabaddccadbcabdadcdcddbaacaccadcdadaddcbacddcdbddcbacacacdccaabbaddcddabacbacbbdcdacbabaacdbcacdcaacacdcbcccaabacccdcbaadbcdccabadbbacdaaacdbabccbcccbaabcacdcacaadacadbabaadcacdcbbdbaacbcabaabaddcbcacdadcdcbcbcaadaaacadddacdcdacbacaaccbdcddacdadccccabdbcabbcdbdcaaabccccbbacacdaccbc");
    input.push_back("badadbbacdbbcaabacdbcaddacaacddbbcbcbccccbccdbcbdbaabacababccdbcdcaabcccbabbdcccbcbaddccadcaabdaddcdbaadcaccaadcdadacddcbacdcdcddbddbcbabcaacdccaaabbddcdaababacbbddcdadbcaaabcdbcccdcaacacdcbcccaabacccdcbaadbcdccabadbaacdcaadbaccbccbaabcdaddcaacaadacadbcdabaadcaccbbdbaacbcabaababaddcbcacdaddcbccaadaacadddaccdacbacdaacbdcddacdadccccbdbccabcdbdcaaabacccbcbbacdacdacbc");
    input.push_back("badadbbacdbbcaabacdbccaddacacddbbcbcbccdbccdbcbdbaaabacaabbccdbcdcaabccacbababbdcccbcabaddccadbcaabddcdcdbdbadcaccaadddacddacdcdddbdbcbabcacacdccaaabbddcdaabacbacbdcdacdbaacdcccdbcacadcdcbcccaabacccdcbaadbcdcabadbbaadcaaacdbabccbcccbaabcdcddcaacaadacadbcdabaadcacdbbdaacbcbaabaaddbcbcacdaddcbcbcacadaaacadddacdccdacbacdaaccbdcddcdadccadbccabbcdbdcaaabacccbbacdacdaccbc");
    input.push_back("bdadbacdbaabacdbccadacaacddbbcbcbccdccbcbdbaaabacaababccdbcdcaabccacbababbdcccbcaaddcadbcaabdadcdcddbadcaccaadddacdcbacdcdcddbddbcbabccacdccaaabbaddcddabacbacbbddcacdbabaabcdbccacdbcaacdcdcbccaabacdcbaadbcdccababaacdcaaacdbcbcccbaabcdacddcaacadacadcdabaadcacbbdbaacbbaaabaddbccacdadcdcbbcacadaacadddacdccdacacdaccbdcddacdadccccabdbccabbcdbdcaaabacccbcbacdacacbc");
        Set_random(semillas[i]);

     AL _AL4(input, alphabet);
    t0=clock();
    solucion = _AL4.run();
    t1=clock();
    time = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "405_10 => Fitness: " << solucion.size() << endl;
    cout << "Tiempo: " << time <<endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    input.clear();
    // => 405_15:
    input.push_back("adadbbacdbbcaacbccacaacddbbbcbccdccbcdbcbdbaaabacabbdbcdcaabccacbaabbdcccbcabaddccabcadadccdbdbaadcaccaadcdadaddbaccdcddbddbcbabcacacdcaabbaddcdabacbcbbddcdbcbbcdbcacdbcaacadcdcbcccaaacccbaadbcdccabadbbaacdcaaacdbabcbcccaabcdcddcaaaadacbcdabaadcacdcbbdbabcababaaddbcbacdadcdcbcccadaaacadddacccdcadaacccddcdadccccabdbcabcbdcaaaaccbcbbacdacdacc");
    input.push_back("bdadabcaabacdbccddacaacddbcbcbcdcccbcbdbaabaaabbcdcdcaabccacbbabdcccbcbddcadbcaabdadcdcdbdbaadcaccaacddaddcbacdcdcddbddbcbabcaacdccaaabbaddcddaaacbacbddcdacbaacbccacdbacadcdcbccaaacccdbaadbcdccaadbbacdaacdbabccbccbaabcdcddcaaaadacadbcdabaadcacdcbbdbaccabaabaaddcbcacdadcdcbbcadaaaadddacdcdacbcdaaccbdcddacdadcccadbabbcdaabaccbcbadacdaccbc");
    input.push_back("bdadbbacdbbcaabacdbccaddacaacddbbcbcbccdccbcdcbdaaacaababccdbcdcabcacbababbdccbcbadcadcabdadcdcdbdbaadcaccaacadaddcbacdccddddbcbabcaacdcaaabbadcdaabcacbbddcdaabaacdbccacdbcaaadcdcbcccaabaccdcbaadcdccbadbbaccaaacdbabcbcccbaabcdacdcacaadcadbcdabacacdcbbdbacabaabbddbcbcacdadcdbcbccadaaacdddacdccacbacdaccbcddacdadccccabdbccbcdbcaaacccbcbbacdacdaccbc");
    input.push_back("baddbbdbaabacdbccadaacddbbcbccdbccdbcbdbaabacababccdbcdcabccacbababbdcccbcabaddccadbcaadadddbdbaadcaccaacadcddcacdcdcdbdbcbbacacccaabbadcddaabacbcbbddcdacbcabaabcdbccacdbacaddcbcccaabacccdcbadcdccababacdcaaacdbabccccbaabcdddcaacaadacadbcdabaadcadcbdbaccabaaabddcbcacdadcdcbcbcadaaaadddacdccdacbcdaaccbdcddacdadcccabdbccabdbdcaacccbbbacdacdccb");
    input.push_back("badadbbacdbbcaadbccaddacaacddbbcbcbcdccbccbcbbaaabacaababccdcabcccaabbdccbcabaddcadcaabdadcdcbbaadcaccaadcdadacbacddcddbdbbabacacccaaabaddcddaabacbacbbdddbcabaacdbccacdbcacadcdcbccaabacccdcbadbcdccabadbbaacdcaadbabcccccbacdacddcaaadacadbcdaaadcacdcbbdbaaccababbaddbcbcacdadcdcbcbcaadaaacaddddcacbacdaaccbdcddacdadcccadbccabcdbcaacccbbbacacdaccbc");
    input.push_back("badadbbacdbcaabcdbccaddacaacddbbcbcbccdccbccbdbaaabaaabaccdccaabcccbababbdcccbcabadccabcaabdadcdcdbdbaadcacaadcdadacddcbacdcdcddbdbcbabcacacdccaabbaddcdaabaccbdcdacdbcabacdbcccdbcaacaddcccaabacccdcbaabcdabadbbaacdcaaacdbabcccccbaabcdacddcaacaacadbcdabadacdbbdbacbcabaababaddbcbcacdadcdcbcbcacadaaacaddacdccdacacdacccddcdcccabbccabbcdbdcaaabacccbcbdacdaccbc");
    input.push_back("adadbadbbaabacdcadacaacddbbcbcbccdccbcdbbdbaaacabbccdbcdcaabccacbbabbdccabaddccadbcabdadcdcddbaacaccaddadaddcbacddcbddcbacaacdccaabbaddcddabacbacbbdcdacbabaacdbcacdaacacdcbcccaabacccdcbaadbcdccabadbbacdaaacdbabccbcccbaabcacdcacadacadbabaadcacdcbbdbaacbcababddcbcacdadcdcbcbcaadaaacadddacdcdacbacaaccbdcddacdadcccabdbcabbcdbdcaaabcccbbacacdaccbc");
    input.push_back("badadbbacdbbcaabacdbcaddacaacddbbcbcbcccbccdbcbdbaacababcdbcdcaabcccbabbdcccbcbaddccadabdadddbaadcaccaadcadacddcbacdcdcddbddbcbabcacdccaaabbddcdaababacbbddcdadbcaaabcdbcccdcaacacdcbcccaabacccdcbaadbcdccabadbaacdaadbaccbccbaabdaddcaacadacadbcdabaadcaccbbdbaacbcabaababaddcbcacdadcbcaadaacaddaccdacbcdaacbdcddacdadccccbdbccabcdbdcaaabacccbcbacdacdacbc");
    input.push_back("badadbbacdbbcabacdbccaddacacddbbcbcbccdbccdbcbdbaaabacaabbcdcabccacbababdccccbaddccdbcaabddcdcdbdbacaccaaddacddacdcddbdbcbabcacacdccaaabbdddaabacacbdcdacdbaacdcccdbcacadcdcbcccaabacccdcbadbcdcabadbbaadcaacdbabccbcccbaabcdcddcaacaadacadbcdabaadcacdbbdaacbcbaabaaddbcbcacdaddcbccaadaaacadddacdccdacbacdaaccbdcddcdadccadbccabbcdbdcaaabacccbbacdacdaccbc");
    input.push_back("bdadbacdbaabacdbcadacaacddbbbcbccdccbcbdbaaabacaababcdbcdcaabccacbababbdcccbaaddcadbcaabdadcdcddbadcaccaadddacdcbacdcdcddbddbcbabcacdcaaabaddcddabacbacbbddcacdbabaabcdbccacdbcaacdcdccaabacdcbaadbcdccaabaacdcaaacdbcbcccbaabcdacddcaacadacadcdabaadcacbdbaacbbaaabaddbccacdadcdcbbccadaacadddacdccacacdaccbdcddacdadcccabdbcabbcdbdcaaabacccbcbacdacacbc");
        Set_random(semillas[i]);

       AL _AL5(input, alphabet);
    t0=clock();
    solucion = _AL5.run();
    t1=clock();
    time = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "405_15 => Fitness: " << solucion.size() << endl;
    cout << "Tiempo: " << time <<endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    input.clear();
    // => 405_20:
    input.push_back("aadbbacdbcaacccacaacdbbbcbccdccbcdbcbdbaaabacabbdbcdcaabccacbaabbdccbcabaddcabcadadccddbacacaadcdadaddbaccdcddbddbcbabccacdcaabadddabacbcbbddcdbcbbcdbcacdbcaacdcdcccaaacccbaadbcdccabadbbaadcaacdbabcbcccaabcdcddcaaadcbcdabaadacdcbbdbabcababaaddbcbacdadcdcbcccadaaacadddcccdcadaacccdddadccccabdbcabcbdcaaaaccbcbbacdacdacc");
    input.push_back("bdadabcaabacdbccddacaacddbcbcbcdcccbcbdbaabaaabbcdcdcaabcacbbabcccbcbddcabaabadcdcdbbaadcaccaacdacbacdcdcdbddbbabcaacdccaaabbaddcddaaacbacbddcdacbaacbcacdbacadcdcbccaacccdbaadbcdccaadbbacdaacdbabccbccbaabcdcddcaaaadcadbdabaadacdcbdbaccababaadcbcacdadcdcbbadaaaadddacdcdacbcdaaccbdcddacdadcccdbabbdaabaccbcbadacdaccbc");
    input.push_back("bdabbacdbbcaabacdbccaddacaacddbbcbcbcdccbcdcbdaacaababccbcdcabcacbababbdccbcbadcdcabdadcdcdbdbaccaaadaddcbacdccddddbcbabaacdcaaabbadcdaacacbbddcdaabaacdbccacdbcaacdbcccaabaccdcaadcdccbadbbaccaacdbabcbcccbaabcdacdcacaadcadbcdabacacdcbbdbacabaabbddbcbcacdadcdbcbccadaaacdddacdccacbacdaccbcddacdadccccabbccbcdcaaacccbcbbacdacdaccbc");
    input.push_back("baddbdbaabacdbccadaacdbcbcdccdbcbdbabacababccdbcdcaccacbababbdcccbcabadccadbcaadadddbdbaadcacaacadcddcacdcdcddbcbbaccccaabbadcddaabcbcbbddcdacbcabaabcdbccacdbacaddcbcccaabaccdcbadcdcbabacdcaaacdbabccccbaabcdddcaacadacadbcdabaadcacbdbaccabaaabddcbcacdadcdcbcbcadaaaaddaccdabcdaacbdcddacdadcccabdbccabdbdcaacccbbacdacdccb");
    input.push_back("bdbbacdbbcaadbccaddacaacddbbcbcbcdccbccbcbbaabacaaabcdcabcccaabbdccbcabaddcadcaabdadcdcbbaadcaccaadcdadcbacddcddbdbbabacaccaaabaddcddaabacbacbbdddbcabaacdbccacdbcacadcdcbcaaacccdcbadbcdcabadbbaacdcaadbabcccccbacdacddcaaadcadbcdaaadcacdcbbdbaaccababbddbcbcacdaddbcbcaadaaacaddddcacbacdaaccbdcddacdadcccadbccabcdbcaacccbbbacacdacbc");
    input.push_back("bdadbbacdbcaabcdbccaddacaacddbbcbcbccdccbccbbaaabaaabaccdccaabccbababbdcccbcabadccabcaabdadcdcdbdbaadcacaadcadacddcbacdcdcddbdbcbabcacacdccaabbadcdaabaccbdcdacdbcabacdbcccbcaacaddcccaabacccdcbabcdabadbbaacdcaacdbabcccccbaacdacdcaacaacadbcdabadacdbbdbacbcabaaabaddbcbacdadcdcbcbcacadaaacadacdccdacacdacccddcdcccabbccabbcdbdcaaabaccbcbdacdaccbc");
    input.push_back("adadbadbbaabacdcadacaacddbbcbcbccdcbcdbdbaaacabbccdbcdcaabccacbbabbdccabadccababdadcdcddaacaccadddaddcbacddcbddcbacaacdcaabbaddcddabacbacbbdcdacbabaacdbcacdaacaccbccaabacccdcaadbcdccabdbbacdaaacdbabccbcccbaacacdcacadacadbabaadcacdcbbbaacbcababdcbcacdaddcbcbcaadaaacadddcdcdcbacaaccbdcddacdadccabdbcabbcdbdcaaabcccbbacacdaccbc");
    input.push_back("badadbbacdbbcaabacdbcaddacaacddbbcbbcccbccdbcbdbcababcbcdcaabcccabbdcccbcbdccadabddddbaadcaccaadcadacddcacdccddbddbcbabcacdccaaabbddcdaababcbbddcdadbcaaabcdbcdcaacacdcccaabccdcbaadbcdccabadbaacdaadbaccbccbabdaddcaacadacadbcdabaadcaccbbdbaacbcabababaddcbcacdadcbcadaacaddaccdacbcdaacbdcddacdadccccbdbccabcdbdcaaabacccbcbacdacdacbc");
    input.push_back("babbadbbcabacdbccaddacadbbcbcbccdbccdbcbdbaaabacaabbcdcbccacbababdccccbadccdbcaabddcdcdbdbacaccaddaddacdcddbdbcbabcacacdccaabdddaabacacbdcdacdbaacdcccdbcacadcdcbccabaccdcadcdcabadbbaadcaacdbabcccccbaabccddcaacaadacadbcdabaacacdbbdaacbcbaabaaddbcbcacdaddcbccaadaaacadddacdcccbacdaaccbdcddcdadccadbccabbcdbdaaabacccbbacdacdaccb");
    input.push_back("bdadbacdbaabacdcadacaacddbbcbccdccbcbdbaaabaababcdbcdcaabccacbababbdcccaadadbcaabdadcdcddbadcacaadddacdcbacdccddbddbcbabcacdcaaabddcddbabacbbddccdbabaabcdbccacbcaacdcdccaabacdcaaddccaabaaccaaacdbcbcccbaabcdacddcaaaacadcdabaadcacbdbacbbaaabaddbacdadcdcbbccaaacdddacdccaccdaccbdcdaadccabbcabbcdbdcaaabacccbcbacdacacbc");
           Set_random(semillas[i]);
   AL _AL6(input, alphabet);
    t0=clock();
    solucion = _AL6.run();
    t1=clock();
    time = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "405_20 => Fitness: " << solucion.size() << endl;
    cout << "Tiempo: " << time <<endl;

}
}