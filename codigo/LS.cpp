
#include "LS.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LS::LS(vector<string> input, string alphabet){
    this->alphabet = alphabet;
    this->input = input;
    //this->initialize_solution_permutation();
    this->initialize_solution_MM();
}
LS::LS(string solution, vector<string> input, string alphabet){
    this->alphabet = alphabet;
    this->input = input;
    this->solution = solution;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LS::initialize_solution_permutation(){
    int max = 0;
    for( int i=0 ; i<input.size(); i++){
        if (input[i].size()>max){
            max = input[i].size();
        }
    }
    this->solution.clear();
    for(int i=0; i<max; i++){
        this->solution.append(this->alphabet);
    }
    //Probar a iniciar tambien con el MM
}

void LS::initialize_solution_MM(){
    this->solution = "";
    this->repair_sol(this->solution);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LS::repair_sol(string& solution){
    vector<string> input_copy = this->input;
    vector<vector<int>> LEMatrix;
    vector<int> productivity;
    vector<int> aux(solution.size(),0);
   
    for(auto&& x : input_copy)
        LEMatrix.push_back(aux);

    for(int i = 0; i < input_copy.size(); i++){
        
        int step = 0;
        //Calcular matriz leftmost embedding y eliminar los caracteres del input que estan incrstados en la solucion -> resultado: caracteres que no estan incrustados
        while(step < solution.size()){
            if(!input_copy[i].empty()){
                if(input_copy[i][0] == solution[step]){                    
                    LEMatrix[i][step] = 1;                    
                    input_copy[i].erase(input_copy[i].begin());                    
                }
            }
            //cout << step << endl;
            step++;
        }
    }

    //Calculamos un vector de productividad
    for(int j = 0; j < LEMatrix[0].size(); j++){
        int sum = 0;
        for( int i = 0; i < LEMatrix.size(); i++){
            sum += LEMatrix[i][j];
        }
        productivity.push_back(sum);
    }

    //Posiciones con productividad = 0 -> eliminar caracter de la solucion
    for(int i = 0; i < productivity.size(); i++){
        if( productivity[i] == 0){
            solution.erase(solution.begin()+i);
            productivity.erase(productivity.begin()+i);
            i--;
        }
    }

    //Comprobamos si input ha quedado vacío
    bool inputEmpty = true;
    for(int i = 0; i < input_copy.size() && inputEmpty; i++){
        if(!input_copy[i].empty()){
            inputEmpty = false;
        }
    }

    //Si input esta vacio -> solution es factible ; si no esta vacio -> necesario reparacion -> ejecutamos MM con el input restante
    if(!inputEmpty){
        MM _MM(input_copy, this->alphabet);
        string repa = _MM.run();
        solution.append(repa);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

string LS::generate_neighbor(){

    for(int i = 0; i < this->solution.size(); i++){
        string neighbor = this->solution;
        neighbor.erase(neighbor.begin()+i);
        this->repair_sol(neighbor);
        if (neighbor.size() < this->solution.size())
            return neighbor;
    }
    return "0";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

string LS::run(){
    string neighbor =  this -> generate_neighbor();
    while( neighbor != "0"){
        neighbor =  this -> generate_neighbor();
        if(neighbor != "0")
            this->solution = neighbor;
    }
    return this->solution;
}