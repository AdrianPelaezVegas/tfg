#include "MABS.h"

MA::MA(vector<string> input, string alphabet){
    this->input = input;
    this->alphabet = alphabet;
    // Lower bound sera la 
    this->LowerBound = 0;
    for(auto it = this->input.begin(); it != this->input.end(); it++){
        if(it->size() > this->LowerBound)
            this->LowerBound = it->size();
    }
    this->UpperBound = this->alphabet.size() * this->LowerBound;

}

void MA::initialize_random_pop(){
    int popSize = 100;
    for(int i=0; i<popSize; i++){
        string aux;
        int chrSize = Randint(this->LowerBound, this->UpperBound);
        for(int j=0; j<chrSize; j++){
            aux.push_back(this->alphabet[Randint(0,this->alphabet.size()-1)]);
        }
        this->pop.push_back(aux);
    }
}

void MA::repair_sol(string& solution){

    vector<string> input_copy = this->input;
    vector<vector<int>> LEMatrix;
    vector<int> productivity;
    vector<int> aux(solution.size(),0);

   
    for(auto&& x : input_copy)
        LEMatrix.push_back(aux);


    for(int i = 0; i < input_copy.size(); i++){
        
        int step = 0;
        //Calcular matriz leftmost embedding y eliminar los caracteres del input que estan incrstados en la solucion -> resultado: caracteres que no estan incrustados
        while(step < solution.size()){
            if(!input_copy[i].empty()){
                if(input_copy[i][0] == solution[step]){
                    
                    LEMatrix[i][step] = 1;
                    
                    input_copy[i].erase(input_copy[i].begin());
                    
                }
            }
            //cout << step << endl;
            step++;
        }
    }


    //Calculamos un vector de productividad
    for(int j = 0; j < LEMatrix[0].size(); j++){
        int sum = 0;
        for( int i = 0; i < LEMatrix.size(); i++){
            sum += LEMatrix[i][j];
        }
        productivity.push_back(sum);
    }


    //Posiciones con productividad = 0 -> eliminar caracter de la solucion
    for(int i = 0; i < productivity.size(); i++){
        if( productivity[i] == 0){
            solution.erase(solution.begin()+i);
            productivity.erase(productivity.begin()+i);
            i--;
        }
    }
    //Comprobamos si input ha quedado vacío


    bool inputEmpty = true;
    for(int i = 0; i < input_copy.size() && inputEmpty; i++){
        if(!input_copy[i].empty()){
            inputEmpty = false;
        }
    }


    //Si input esta vacio -> solution es factible ; si no esta vacio -> necesario reparacion -> ejecutamos MM con el input restante
    if(!inputEmpty){
        MM _MM(input_copy, this->alphabet);
        string repa = _MM.run();
        solution.append(repa);
    }


}

void MA::repair_pop(){
    for(int i = 0; i < this->pop.size(); i++){
        this->repair_sol(this->pop[i]);
    }
}

int MA::evaluate_sol(string sol){
    return sol.size();
}

void MA::evaluate_pop(){
    this->eval.clear();
    for(auto it = this->pop.begin(); it != this->pop.end(); it++)
        this->eval.push_back(this->evaluate_sol(*it));
}

string MA::binary_tournament_selection(){
    int cand1 = Randint(0,this->pop.size()-1);
    int cand2 = Randint(0,this->pop.size()-1);
    string ret;

    if(pop[cand1].size() < pop[cand2].size()){
        ret = pop[cand1];
        pop.erase(pop.begin()+cand1);
    }
    else{
        ret = pop[cand2];
        pop.erase(pop.begin()+cand2);
    }
    return ret;
}

void MA::uniform_cross_over(string p1, string p2, string& c1, string& c2){
    
    int i_p1 = 0; int i_p2 = 0;

    c1.clear(); c2.clear();

    while(!p1.empty() && !p2.empty()){
        int x = Randint(0,1);
        if (x == 0){
            c1.push_back(p1[0]);
            c2.push_back(p2[0]);
        }
        else{
            c1.push_back(p2[0]);
            c2.push_back(p1[0]);
        }
        p1.erase(p1.begin());
        p2.erase(p2.begin());
    }
    //if(!p1.empty())
    // La parte restante de uno de los padres( el ams largo ) no se tiene en cuenta.
}

void MA::mutate(string& c){
    float Pm = 0.01;
    int n_gen_mut = rint(c.size()*Pm);
    for(int i=0 ; i<n_gen_mut; i++){
        float pos = Randint(0,c.size()-1);
        float alph = Randint(0,this->alphabet.size()-1);
        while(c[pos] == this->alphabet[alph])
            alph = Randint(0,this->alphabet.size()-1);
        c[pos] = this->alphabet[alph];
    }
}

vector<string> MA::select_2_best(string p1, string p2, string c1, string c2){
    int max = 9999999;
    int index = 0;
    vector<string> aux;
    aux.push_back(p1);
    aux.push_back(p2);
    aux.push_back(c1);
    aux.push_back(c2);
    vector<string> ret;
    for(int i = 0; i < aux.size(); i++){
        if(aux[i].size() < max){
            max = aux[i].size();
            index = i;
        }
    }
    ret.push_back(aux[index]);
    aux.erase(aux.begin()+index);
    max = 999999;
    index = 0;
    for(int i = 0; i < aux.size(); i++){
        if(aux[i].size() < max){
            max = aux[i].size();
            index = i;
        }
    }
    ret.push_back(aux[index]);

    return ret;
}

string MA::best_of_pop(){
    int min = 999999;
    int index = 0;
    for(int i=0; i<this->pop.size(); i++){
        if (this->pop[i].size() < min){
            min = this->pop[i].size();
            index = i;
        }
    }
    return this->pop[index];
}

string MA::run(){
    
    this->initialize_random_pop();
    
    this->repair_pop();
    
    int stop = 0;
    int nEval = 10000;
    float Pls = 0.01;
    int nLS = rint(nEval * Pls);
    int x = nEval / nLS;
    int lsCount = 0;
    unsigned t0,t1;
    double time;
    t0 = clock();
    while(stop < nEval){

        string parent1 = this->binary_tournament_selection();
        string parent2 = this->binary_tournament_selection();

        string child1;
        string child2;

        float cross = Randfloat(0,1);
        if(cross < 0.9){
            this->uniform_cross_over(parent1, parent2, child1, child2);
            this->repair_sol(child1);
            this->repair_sol(child2);
        }
        else{
            child1 = parent1;
            child2 = parent2;
        }

        this->mutate(child1);
        this->mutate(child2);
        this->repair_sol(child1);
        this->repair_sol(child2);
        
        vector<string> replace = this->select_2_best(parent1, parent2, child1, child2);

        this->pop.push_back(replace[0]);
        this->pop.push_back(replace[1]);

        stop++;
        lsCount++;

        if(lsCount == x){
            string x = this->best_of_pop();
            
            t1 = clock();
            time = (double(t1-t0)/CLOCKS_PER_SEC);
            cout << time << " " << x.size() << endl;

            /*
            for(int i = 0; i < this->pop.size(); i++ ){
                LS _LS(this->pop[i],this->input, this->alphabet);
                this->pop[i] = _LS.run();
            }
            */
            int index = Randint(0,this->pop.size()-1);
            LS _LS(this->pop[index],this->input, this->alphabet);
            this->pop[index] = _LS.run();
            lsCount = 0;
        }
        
            
        
    }

    return this->best_of_pop();

}