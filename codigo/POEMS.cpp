
#include "POEMS.h"
#include<ctime>
//////////////////////////////////////////////////////////////////////////////////////////

//Metodos random:
/*
#define MASK 2147483647
#define PRIME 65539
#define SCALE 0.4656612875e-9

unsigned long Seed = 0L;
void Set_random (unsigned long x){
    Seed = (unsigned long) x;
}
float Rand(void){
    return (( Seed = ( (Seed * PRIME) & MASK) ) * SCALE );
}
int Randint(int low, int high){
    return (int) (low + (high-(low)+1) * Rand());
}
float Randfloat(float low, float high){
    return (low + (high-(low))*Rand());
}
*/
//////////////////////////////////////////////////////////////////////////////////////////

GA::GA(int maxGen, int N, string prototype, string alphabet, vector<string> input){
    this->maxGen = maxGen;
    this->N = N;
    this->prototype = prototype;
    this->alphabet = alphabet;
    this->input = input;
    this->sumImput = 0;
    for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++) 
        this->sumImput += it->size(); 
    this->Pcross = 0.75;
    this->Pmut = 0.25;
}

//////////////////////////////////////////////////////////////////////////////////////////

action GA::random_action(int limit){
    action a;
    int x = Randint(0,3);

    if(limit == -1){
        a.typeAction = 'I';
        a.letter = this->alphabet[Randint(0, this->alphabet.size()-1)];
        a.id1 = 99999;
    }
    else{
        switch(x){ 
            case 0: 
                a.typeAction = 'N';
                break;
            case 1: 
                a.typeAction = 'M';
                //No se controla por ahora que en los ids pueda salir el mismo numero
                a.id1 = Randint(0, limit);
                a.id2 = Randint(0, limit);
                break;
            case 2: 
                a.typeAction = 'I';
                a.letter = this->alphabet[Randint(0, this->alphabet.size()-1)];
                a.id1 = Randint(0, limit);
                break;
            case 3:
                a.typeAction = 'D';
                a.id1 = Randint(0, limit);
                break;
            default: break;
        }
    }
    return a;
}

//////////////////////////////////////////////////////////////////////////////////////////

vector<action> GA::random_chromosome(){
    vector<action> ch;
    action a;
    int limit = this->prototype.size()-1;
    for(int i=0; i<this->maxGen; i++){
        a = this->random_action(limit);
        ch.push_back(a);
        if(a.typeAction == 'D')
            limit--;
        else if(a.typeAction == 'I')
            limit++;
    }
    return ch;
}

//////////////////////////////////////////////////////////////////////////////////////////

void GA::initialize_pop(){
    for(int i = 0; i < this->N; i++){
        this->pop.push_back(this->random_chromosome());
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

int GA::alignMod(string cad){
    int k = 0;
    int l = 0;
    int ret = 0;

    while(k < this->prototype.size() && l < cad.size()){
        if(this->prototype[k] == cad[l]){
            ret++;
            l++;
        }
        k++;
    }
    return ret;
}

//////////////////////////////////////////////////////////////////////////////////////////

int GA::alignMod(string cad, string prot){
    int k = 0;
    int l = 0;
    int ret = 0;

    while(k < prot.size() && l < cad.size()){
        if(prot[k] == cad[l]){
            ret++;
            l++;
        }
        k++;
    }
    return ret;
}

//////////////////////////////////////////////////////////////////////////////////////////

float GA::fitness(){
    int C = 0;
    float S = 0;

    for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++)
        C += this->alignMod(*it);
    
    S = (this->sumImput - this->prototype.size()) / this->sumImput;

    return C+S;
}

//////////////////////////////////////////////////////////////////////////////////////////

float GA::fitness(string prot){
    float C = 0;
    float S = 0;

    for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++)
        C += this->alignMod(*it, prot);
    
    float sumL = this->sumImput;
    S = (sumL - prot.size()) / sumL;

    float result = C+S;

    return result;

}

//////////////////////////////////////////////////////////////////////////////////////////

string GA::apply_action(action a){
    string aux = this->prototype;
    switch (a.typeAction)
    {
        case 'N':            
            break;
        case 'M':
        {
            char l = aux[a.id1];
            aux.erase(aux.begin()+a.id1);
            aux.insert(aux.begin()+a.id2+1, l);
        }
            break;
        case 'I':
        {
            aux.insert(aux.begin()+a.id1+1, a.letter);
        }
            break;
        case 'D':
        {
            aux.erase(aux.begin()+a.id1);
        }
            break;    
        default:
            break;
    }
    return aux;
}

//////////////////////////////////////////////////////////////////////////////////////////

string GA::apply_action(action a, string prot){
    string aux = prot;
    int tam = aux.size();
    switch (a.typeAction)
    {
        case 'N':            
            break;
        case 'M':
        {
            char l = aux[a.id1];
            if(a.id1 < a.id2 && a.id2 < tam){
                if(a.id2 == tam-1)
                    aux.push_back(l);
                else
                    aux.insert(aux.begin()+a.id2+1, l);
                aux.erase(aux.begin()+a.id1);                                
            }
            else if(a.id2 < a.id1 && a.id1 < tam){
                aux.insert(aux.begin()+a.id2+1, l);
                aux.erase(aux.begin()+a.id1+1);
            }
        }
            break;
        case 'I':
        {
            if(a.id1 < tam-1)
                aux.insert(aux.begin()+a.id1+1, a.letter);
            else
                aux.push_back(a.letter);
        }
            break;
        case 'D':
        {
            if(tam>0 && a.id1<tam)
                aux.erase(aux.begin()+a.id1);
            else if(tam>0)
                aux.pop_back();
        }
            break;    
        default:
            break;
    }
    return aux;
}

//////////////////////////////////////////////////////////////////////////////////////////

string GA::apply_seq_actions(vector<action> seq, string prot){
    string aux = prot;
    for(vector<action>::iterator it_action = seq.begin(); it_action != seq.end(); it_action++){
        aux = this->apply_action(*it_action, aux);
    }

    return aux;
}

//////////////////////////////////////////////////////////////////////////////////////////

void GA::evaluate_pop(){
    this->eval.clear();
    string aux = this->prototype;
    
    for(vector<vector<action>>::iterator it_sequence = this->pop.begin(); it_sequence != pop.end(); it_sequence++){
        /*
        for(vector<action>::iterator it_action = it_sequence->begin(); it_action != it_sequence->end(); it_action++){
            aux = this->apply_action(*it_action, aux);
        }
        */
        aux = this->apply_seq_actions(*it_sequence, aux);
        this->eval.push_back(this->fitness(aux));
        aux = this->prototype;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

float GA::evaluate_chromosome(vector<action> seq){
    string aux = this->apply_seq_actions(seq,this->prototype);
    float fit = this->fitness(aux);
    return fit;
}

//////////////////////////////////////////////////////////////////////////////////////////

vector<action> GA::best_sequence(){
    float max = -1;
    int pos = 0;
    for(int i=0; i<this->eval.size(); i++){
        if(eval[i]>max){
            max = eval[i];
            pos = i;
        }
    }
    return this->pop[pos];
}

//////////////////////////////////////////////////////////////////////////////////////////

vector<action> GA::select(){
    int c1 = 0;
    int c2 = 0;

    while(c1 == c2){
        c1 = Randint(0, this->pop.size()-1);
        c2 = Randint(0, this->pop.size()-1);
    }
    if(this->eval[c1] >= this->eval[c2])
        return this->pop[c1];
    else 
        return this->pop[c2];
}

//////////////////////////////////////////////////////////////////////////////////////////

vector<action> GA::cross_over(vector<action> p1, vector<action> p2){
    vector<action> child;
    for(int i=0; i<this->maxGen; i++){
        int a = Randint(0,1);
        if (a == 0)
            child.push_back(p1[i]);
        else
            child.push_back(p2[i]);
    }
    //Modificar la secuencia de acciones hijo teniendo en cuenta el tamaño que tendría el prototipo tras la aplicación de cada acción.
    return child;
}

//////////////////////////////////////////////////////////////////////////////////////////

void GA::mutate(vector<action>& child){
    int limit = this->prototype.size()-1;
    for(int i=0; i<child.size(); i++){
        float p = Randfloat(0,1);
        if(p<this->Pmut){
            child[i] = this->random_action(limit);
        }
        if(child[i].typeAction == 'D')
            limit--;
        else if(child[i].typeAction == 'I')
            limit++;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

string GA::run_genericGA(){
    
    vector<action> best;
    vector<vector<action>> newPop;
    int num_eval = 0;
    this->initialize_pop();
    this->evaluate_pop();
    best = this->best_sequence();

    while(num_eval < 1000){
        newPop.push_back(best);
        while(newPop.size() < this->N){
            vector<action> p1 = this->select();
            vector<action> p2 = this->select();
            vector<action> child1;
            vector<action> child2;
            this->cross_over(p1,p2,child1,child2);
            this->mutate(child1);
            this->mutate(child2);
            newPop.push_back(child1);
            newPop.push_back(child2);
        }
        this->pop = newPop;
        newPop.clear();
        this->evaluate_pop();
        num_eval = num_eval + this->N;
        best = this->best_sequence();
    }
    return this->apply_seq_actions(best, this->prototype);
}

//////////////////////////////////////////////////////////////////////////////////////////

action GA::random_active_action(int limit){
    action a;
    int x = Randint(0,2);

    if(limit == -1){
        a.typeAction = 'I';
        a.letter = this->alphabet[Randint(0, this->alphabet.size()-1)];
        a.id1 = 99999;
    }
    else{
        switch(x){             
            case 0: 
                a.typeAction = 'M';
                //No se controla por ahora que en los ids pueda salir el mismo numero
                a.id1 = Randint(0, limit);
                a.id2 = Randint(0, limit);
                break;
            case 1: 
                a.typeAction = 'I';
                a.letter = this->alphabet[Randint(0, this->alphabet.size()-1)];
                a.id1 = Randint(0, limit);
                break;
            case 2:
                a.typeAction = 'D';
                a.id1 = Randint(0, limit);
                break;
            default: break;
        }
    }
    return a;
}

//////////////////////////////////////////////////////////////////////////////////////////

vector<action> GA::random_chromosome_niche(int i){
    vector<action> ch;
    action a;
    int limit = this->prototype.size()-1;
    int nActiveActions = 0;
    for(int i=0; i<this->maxGen; i++){

        if(nActiveActions < i){
            a = this->random_active_action(limit);
            ch.push_back(a);
            if(a.typeAction == 'D')
                limit--;
            else if(a.typeAction == 'I')
                limit++;    
            nActiveActions++;
        }
        else{
            a = this->random_action(limit);
            ch.push_back(a);
            if(a.typeAction == 'D')
                limit--;
            else if(a.typeAction == 'I')
                limit++;
        }
    }
    return ch;
}

//////////////////////////////////////////////////////////////////////////////////////////

void GA::initialize_niches(){
    int nNiches = this->maxGen;
    int sizeNiches = this->N / nNiches;
    
    this->niches.clear();

    for(int i = 1; i <= nNiches; i++){
        vector<vector<action>> nicheAux;
        for(int j = 0; j < sizeNiches; j++){
            nicheAux.push_back(this->random_chromosome_niche(i));
        }
        this->niches.push_back(nicheAux);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

void GA::evaluate_niches(){
    this->nichingEval.clear();
    this->orderedNiches.clear();
    string aux = this->prototype;
    
    for(int i = 0; i < this->niches.size(); i++){
        vector<float> auxVector;
        //multimap<float, vector<action>> auxMultimap;
        this->nichingEval.push_back(auxVector);
        //this->orderedNiches.push_back(auxMultimap);
        for(auto it_sequence = this->niches[i].begin(); it_sequence != this->niches[i].end(); it_sequence++){            
            aux = this->apply_seq_actions(*it_sequence, aux);
            float fit = this->fitness(aux);

            this->nichingEval[i].push_back(fit);
            
            //pair<float,vector<action>> auxPair (fit, *it_sequence);
            //this->orderedNiches[i].insert(auxPair);

            aux = this->prototype;
        }
    }
}
//////////////////////////////////////////////////////////////////////////////////////////

vector<action> GA::niching_tournament_selection(){
    int nNiches = this->maxGen;
    int i = Randint(0, nNiches-1);
    int c1 = 0;
    int c2 = 0;

    while(c1 == c2){
        c1 = Randint(0, this->niches[i].size()-1);
        c2 = Randint(0, this->niches[i].size()-1);
    }

    if(this->nichingEval[i][c1] >= this->nichingEval[i][c2]){
        return this->niches[i][c1];
    }
    else{
        return this->niches[i][c2];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

void GA::cross_over(vector<action> p1, vector<action> p2, vector<action>& child1, vector<action>& child2){
    child1.clear();
    child2.clear();
    float p = Randfloat(0,1);
    if(p < this->Pcross){
        for(int i=0; i<this->maxGen; i++){
            int a = Randint(0,1);
            if (a == 0){
                child1.push_back(p1[i]);
                child2.push_back(p2[i]);
            }
            else{
                child1.push_back(p2[i]);
                child2.push_back(p1[i]);
            }
        }
    }
    else{
        child1 = p1;
        child2 = p2;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

void GA::niching_replacement(vector<action> child){
    int nActiveAction=0;
    for(auto it = child.begin(); it != child.end(); it++){
        if(it->typeAction != 'N')
            nActiveAction++;
    }

    float fit = this->evaluate_chromosome(child);

    bool found = false;
    for(int i = nActiveAction-1; (i >= 0) && (!found); i--){
    //for(int i = 0; i < nActiveAction && !found; i++){  
        for(int j = 0; (j < this->nichingEval[i].size() ) && (!found); j++){
            if(this->nichingEval[i][j] <= fit){
                this->niches[i][j] = child;
                this->nichingEval[i][j] = fit;
                found = true;
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

vector<action> GA::best_sequence_niching(){
    float max = 0;
    int nicho=0;
    int seq=0;
    for(int i = 0; i < this->niches.size(); i++){
        for(int j = 0; j < this->niches[i].size(); j++){
            if(this->nichingEval[i][j] > max){
                max = this->nichingEval[i][j];
                nicho = i;
                seq = j;
            }
        }
    }
    return this->niches[nicho][seq];
}

//////////////////////////////////////////////////////////////////////////////////////////

string GA::run_iEA(){

    int numEval = 0;

    this->initialize_niches();
    this->evaluate_niches();

    while(numEval < 1000){
        vector<action> p1 = this->niching_tournament_selection();
        vector<action> p2 = this->niching_tournament_selection();
        vector<action> child1;
        vector<action> child2;
        this->cross_over(p1, p2, child1, child2);
        this->mutate(child1);
        this->mutate(child2);
        this->niching_replacement(child1);
        this->niching_replacement(child2);

        numEval+=2;

    }
    vector<action> best = this->best_sequence_niching();
    //cout << "a" << endl;
    return this->apply_seq_actions(best, this->prototype);
}

//////////////////////////////////////////////////////////////////////////////////////////

POEMS::POEMS(vector<string> input, string alphabet){
    this->input = input;
    this->alphabet = alphabet;

    this->prototype = "";

    for(vector<string>::iterator it = input.begin(); it != input.end(); it++){
        if(it->size() > this->prototype.size())
            this->prototype = *it;
    }
    this->sumImput = 0;
    for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++) 
        this->sumImput += it->size();
   
  //  cout << "simIMput: " << this->sumImput <<endl;
}

//////////////////////////////////////////////////////////////////////////////////////////

float POEMS::fitness(string prot){
    int C = 0;
    float S = 0;

    for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++)
        C += this->alignMod(*it, prot);
    
    S = (float)(this->sumImput - prot.size()) / this->sumImput;
   // cout << "C: "<<C<<"S: "<<S<<"-> " << C+S << endl;
    return C+S;

}

//////////////////////////////////////////////////////////////////////////////////////////

int POEMS::alignMod(string cad, string prot){
    int k = 0;
    int l = 0;
    int ret = 0;

    while(k < prot.size() && l < cad.size()){
        if(prot[k] == cad[l]){
            ret++;
            l++;
        }
        k++;
    }
    return ret;
}

//////////////////////////////////////////////////////////////////////////////////////////

string POEMS::apply_action(action a, string prot){
    string aux = prot;
    int tam = aux.size();
    switch (a.typeAction)
    {
        case 'N':            
            break;
        case 'M':
        {
            char l = aux[a.id1];
            if(a.id1 < a.id2 && a.id2 < tam){
                if(a.id2 == tam-1)
                    aux.push_back(l);
                else
                    aux.insert(aux.begin()+a.id2+1, l);
                aux.erase(aux.begin()+a.id1);                                
            }
            else if(a.id2 < a.id1 && a.id1 < tam){
                aux.insert(aux.begin()+a.id2+1, l);
                aux.erase(aux.begin()+a.id1+1);
            }
        }
            break;
        case 'I':
        {
            if(a.id1 < tam-1)
                aux.insert(aux.begin()+a.id1+1, a.letter);
            else
                aux.push_back(a.letter);
        }
            break;
        case 'D':
        {
            if(tam>0 && a.id1<tam)
                aux.erase(aux.begin()+a.id1);
            else if(tam>0)
                aux.pop_back();
        }
            break;    
        default:
            break;
    }
    return aux;
}

//////////////////////////////////////////////////////////////////////////////////////////

string POEMS::run(){
    int eval = 0;
    unsigned t0,t1;
    double time;
    t0 = clock();
    while(eval < 3500000){

        GA ga(5, 100, this->prototype, alphabet, input);
        //string best =  ga.run_genericGA();
        string best =  ga.run_iEA();
        eval+=1000;

        float f_best = this->fitness(best);
        float f_pro = this->fitness(this->prototype);
        
        if(f_best >= f_pro){
            
                this->prototype = best;
        }
        
        if(eval%100000 == 0){
            int x = this->prototype.size();
            t1 = clock();
            time = (double(t1-t0)/CLOCKS_PER_SEC);
            cout << time << " " << x << endl;
        }
        
    }
    return this->prototype;
}

//////////////////////////////////////////////////////////////////////////////////////////
