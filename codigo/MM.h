#ifndef MM_H
#define MM_H

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <functional>

using namespace std;
void Set_random (unsigned long x);
float Rand(void);
int Randint(int low, int high);
float Randfloat(float low, float high);

class MM{
    
    private:
        vector<string> input;
        string alphabet;
        map<char,int> frecuencias;

    public:
        MM(vector<string> input, string alphabet);
        string run();

};


#endif