#ifndef LS_H
#define LS_H

#include<iostream>
#include<vector>
#include<map>
#include<set>
#include <algorithm>
#include <functional>

#include "MM.h"

using namespace std;

class LS {

private:

string solution;
string alphabet;
vector<string> input;

public:

LS(vector<string> input, string alphabet);
LS(string solution, vector<string> input, string alphabet);
void initialize_solution_permutation();
void initialize_solution_MM();
void repair_sol(string& solution);
string generate_neighbor();
string run();

};




#endif