#include "AL.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int myrandom (int i) { return std::rand()%i;}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

AL::AL(vector<string> input, string alphabet){
    this->input = input;
    this->maxInputSize = 0;
    for(auto it = this->input.begin(); it != this->input.end(); it++){
        if(it->size() > this->maxInputSize)
            this->maxInputSize = it->size();
    }
    this->alphabet = alphabet;
    this->permutation = this->alphabet;
    random_shuffle(this->permutation.begin(), this->permutation.end(), myrandom);
    for(int i=0; i<this->maxInputSize; i++){
        this->solution = this->solution + permutation;
    }
    //vector<int> aux (this->solution.size(), 0);
    //this->productivity = aux;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AL::leftmost_embbeding(){
    this->LEMatrix.clear();
    vector<int> aux(this->solution.size(),0);
    for(auto&& x : this->input)
        this->LEMatrix.push_back(aux);

    for(int i = 0; i < this->input.size(); i++){
        int pos = 0;
        for(int step = 0; step < this->solution.size(); step++){
            if(this->input[i][pos] == this->solution[step]){
                this->LEMatrix[i][step] = 1;
                pos++;
            }
            if(pos == this->input[i].size())
                break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AL::calculate_productivity(){
    this->productivity.clear();

    for(int j = 0; j < this->solution.size(); j++){
        int sum = 0;
        for(int i = 0; i < this->LEMatrix.size(); i++){
            sum += this->LEMatrix[i][j]; 
        }
        this->productivity.push_back(sum);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AL::delete_unproductive_steps(){
    int i = 0;
    for(auto it = this->solution.begin(); it != this->solution.end(); it++){
        if(this->productivity[i] == 0){
            //Eliminar carecter no productivo de la solucion
            this->solution.erase(it);
            it--;

            //Eliminar de vector productivity
            this->productivity.erase(this->productivity.begin()+i);

            //Eliminar las columnas con productivity=0 de la matrix:
            for(int j=0; j<this->LEMatrix.size(); j++){                
                this->LEMatrix[j].erase(this->LEMatrix[j].begin()+i);
            }
        }
        else{
            i++;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

string AL::run(){

    this->leftmost_embbeding();
    this->calculate_productivity();
    this->delete_unproductive_steps();

    return this->solution;
}