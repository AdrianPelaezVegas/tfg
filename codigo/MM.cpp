#include "MM.h"
#include <random>

//////////////////////////////////////////////////////////////////////////////////////////

//Metodos random:

#define MASK 2147483647
#define PRIME 65539
#define SCALE 0.4656612875e-9

unsigned long Seed = 0L;
void Set_random (unsigned long x){
    Seed = (unsigned long) x;
}
float Rand(void){
    return (( Seed = ( (Seed * PRIME) & MASK) ) * SCALE );
}
int Randint(int low, int high){
    return (int) (low + (high-(low)+1) * Rand());
}
float Randfloat(float low, float high){
    return (low + (high-(low))*Rand());
}

//Metodos distribucion normal:

default_random_engine generator;
normal_distribution<float> distribution(0.0,0.3);

float distribucionNormal(){
  float number = distribution(generator);
  return number;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MM::MM(vector<string> input, string alphabet){
    this->input = input;
    this->alphabet = alphabet;
    for(string::iterator it = this->alphabet.begin(); it != this->alphabet.end(); it++){
        this->frecuencias[*it] = 0;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

string MM::run(){
    string solucion;
    typedef function<bool(pair<char, int>, pair<char, int>)> Comparator;
 
	Comparator compFunctor = [](pair<char, int> elem1 ,pair<char, int> elem2){
		return elem1.second > elem2.second;
	};

    bool isEmpty = false;

    //H1:
    while(!isEmpty){
        
        //Contamos los frecuencias:
        for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++)
            if(!(it->empty()))
                this->frecuencias[*(it->begin())]++;

        //Creamos un multiset ordenado con los valores del map frecuencias, de esta forma el primer elemento del multiset
        // (*.begin()) será el caracter con mayor frecuencia:
        multiset<pair<char, int>, Comparator> frecuenciasOrdenados(this->frecuencias.begin(), this->frecuencias.end(), compFunctor);
        
        //Seleccionamos el caracter con mayor frecuencia:
        bool empate = true;
        vector<char> empates;
        for ( auto it = frecuenciasOrdenados.begin(); it != frecuenciasOrdenados.end() && empate; it++){
            empates.push_back(it->first);
            auto aux = it;
            aux++;
            if( aux->second < it->second)
                empate = false;
        }

        int pos = Randint(0,empates.size()-1);
        char masPeso = empates[pos];
        //char masPeso = frecuenciasOrdenados.begin()->first;

        //Lo añadimos al superstring:
        solucion.push_back(masPeso);

        //Lo eliminamos del front de las cadenas de entrada:
        for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++)
            if(*(it->begin()) == masPeso)
                it->erase(it->begin());

        //Comprobamos que todavía quedan carecteres en alguna cadena:    
        isEmpty = true;
        for(vector<string>::iterator it = this->input.begin(); it != this->input.end(); it++)
            if(!(it->empty()))
                isEmpty = false;

        //Limpiamos las estructuras de datos necesarias:

        for(string::iterator it = this->alphabet.begin(); it != this->alphabet.end(); it++){
            this->frecuencias[*it] = 0;
        }
    }
    return solucion;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////